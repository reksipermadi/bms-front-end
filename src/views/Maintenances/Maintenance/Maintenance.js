import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, requestNo: 'MNT/001/08/19', propertyName: 'HURICANE BUILDING', unitCode: 'A0101', tenantName: 'Reza', equipment: 'AC-10001 -- LG DUALCOOL, 1.5PK', requestDate: '08/20/2019', priority: 'Middle', status: 'open' },
];
const columns = [
  {
    name: 'Request No',
    selector: 'requestNo',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Unit Code',
    selector: 'unitCode',
    sortable: true,
  },
  {
    name: 'Tenant Name',
    selector: 'tenantName',
    sortable: true,
  },
  {
    name: 'Equipment',
    selector: 'equipment',
    sortable: true,
  },
  {
    name: 'Request Date',
    selector: 'requestDate',
    sortable: true,
  },
  {
    name: 'Priority',
    selector: 'priority',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
  </div>
);

class Maintenance extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Maintenance" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Maintenance"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default Maintenance;
