import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, Table, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddMaintenance extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Maintenance" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Maintenance</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="12 mb-2">
                      <h3 className="text-info">Maintenance Header</h3>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="property">Property</Label>
                        <InputGroup>  
                          <Input type="select" name="property" id="property">
                            <option value="0">Choose Property</option>
                            <option value="1">HRC -- HURICANE Building</option>
                            <option value="2">HRCA -- HURICANE APARTMENT</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="unit">Unit</Label>
                        <Input type="select" name="unit" id="unit">
                          <option value="0">Choose Unit</option>
                        </Input>
                        <FormText className="help-block">Please select unit</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="equipment">Equipment</Label>
                        <Input type="select" name="equipment" id="equipment">
                          <option value="0">Choose Equipment</option>
                        </Input>
                        <FormText className="help-block">Please select equipment</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="maintenancePriority">Maintenance Priority</Label>
                        <Input type="select" name="maintenancePriority" id="maintenancePriority">
                          <option value="0">Choose Priority</option>
                          <option value="1">Low</option>
                          <option value="2">Middle</option>
                          <option value="3">High</option>
                        </Input>
                        <FormText className="help-block">Please select priority</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="maintenanceStatus">Maintenance Status</Label>
                        <Input type="select" name="maintenanceStatus" id="maintenanceStatus">
                          <option value="0">Choose Status</option>
                          <option value="1">Open</option>
                          <option value="2">On Progress</option>
                          <option value="3">On Hold</option>
                          <option value="4">Closed</option>
                        </Input>
                        <FormText className="help-block">Please select status</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12 mb-2">
                      <h3 className="text-info">Maintenance Detail</h3>
                    </Col>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="complaintCategory">Complaint Category</Label>
                        <Input type="select" name="complaintCategory" id="complaintCategory">
                          <option value="0">Choose Category</option>
                          <option value="1">CPLT10001 -- Maintenance Air Conditioner</option>
                        </Input>
                        <FormText className="help-block">Please select category</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="complaintIssue">Complaint Issue</Label>
                        <Input type="select" name="complaintIssue" id="complaintIssue">
                          <option value="0">Choose Issue</option>
                        </Input>
                        <FormText className="help-block">Please select issue</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="requestDate">Request Date</Label>
                        <InputGroup>  
                          <Input type="date" id="requestDate" name="requestDate" placeholder="Enter request date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter request date</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="scheduledStart">Scheduled Start</Label>
                        <InputGroup>  
                          <Input type="date" id="scheduledStart" name="scheduledStart" placeholder="Enter scheduled start.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter scheduled start</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="scheduledEnd">Scheduled End</Label>
                        <InputGroup>  
                          <Input type="date" id="scheduledEnd" name="scheduledEnd" placeholder="Enter scheduled end.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter scheduled end</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="actualStart">Actual Start</Label>
                        <InputGroup>  
                          <Input type="date" id="actualStart" name="actualStart" placeholder="Enter actual start.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter actual start</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="actualEnd">Actual End</Label>
                        <InputGroup>  
                          <Input type="date" id="actualEnd" name="actualEnd" placeholder="Enter actual end.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter actual end</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12 mb-2">
                      <h3 className="text-info">Material Usage</h3>
                    </Col>
                    <Col md="12">
                      <Table responsive bordered hover>
                        <thead>
                          <tr>
                            <th>Material Category</th>
                            <th>Material Items</th>
                            <th>@ Price</th>
                            <th>QTY</th>
                            <th>Total Price</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <Input type="select" name="materialCategory" id="materialCategory">
                                <option value="0">Choose Category</option>
                                <option value="1">BAUT-05 -- Baut 0.5 inchi</option>
                                <option value="2">FILTERAC -- Filter Ac</option>
                              </Input>
                            </td>
                            <td>
                              <Input type="select" name="materialItems" id="materialItems">
                                <option value="0">Choose Material</option>
                              </Input>
                            </td>
                            <td>
                              <Input type="text" id="price" name="price" />
                            </td>
                            <td>
                              <Input type="text" id="qyt" name="qyt" />
                            </td>
                            <td>
                              <Input type="text" id="totalPrice" name="totalPrice" />
                            </td>
                            <td>
                              <Button color="danger"><i class="fa fa-trash"></i></Button>
                            </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="6">
                              <Button color="success">
                                <i class="fa fa-plus fa-lg"></i> Material
                              </Button>
                            </td>
                          </tr>
                        </tfoot>
                      </Table>                      
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMaintenance;
