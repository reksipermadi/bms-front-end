import React, { Component } from 'react';
import { Card,  CardBody, Button, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailMaintenance extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Maintenance" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="12 mb-2">
                      <h3 className="text-info">Maintenance Header</h3>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered>
                        <tbody>
                          <tr>
                            <th width="40%">>Property Name</th>
                            <td>HURICANE BUILDING</td>
                          </tr>
                          <tr>
                            <th width="40%">>Unit Code</th>
                            <td>A0101</td>
                          </tr>
                          <tr>
                            <th width="40%">>Equipment</th>
                            <td>AC-10001 -- LG DUALCOOL, 1.5PK</td>
                          </tr>
                          <tr>
                            <th width="40%">>Maintenance Priority</th>
                            <td>Middle</td>
                          </tr>
                          <tr>
                            <th width="40%">>Maintenance Status</th>
                            <td>Open</td>
                          </tr>
                        </tbody>
                    </Table>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12 mb-2">
                      <h3 className="text-info">Maintenance Detail</h3>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered>
                        <tbody>
                          <tr>
                            <th width="40%">Complaint Category</th>
                            <td>CPLT10001 -- Maintenance Air Conditioner</td>
                          </tr>
                          <tr>
                            <th width="40%">Complaint Issue</th>
                            <td>AC-10001 -- AC Keluar Air (Bocor)</td>
                          </tr>
                          <tr>
                            <th width="40%">Request Date</th>
                            <td>08/20/2019</td>
                          </tr>
                          <tr>
                            <th width="40%">Scheduled Start</th>
                            <td>08/20/2019</td>
                          </tr>
                          <tr>
                            <th width="40%">Scheduled End</th>
                            <td>08/20/2019</td>
                          </tr>
                          <tr>
                            <th width="40%">Actual Start</th>
                            <td>08/20/2019</td>
                          </tr>
                          <tr>
                            <th width="40%">Actual End</th>
                            <td>08/20/2019</td>
                          </tr>
                        </tbody>
                    </Table>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12 mb-2">
                      <h3 className="text-info">Material Usage</h3>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered>
                        <tbody>
                          <thead>
                            <tr>
                              <th width="40%">Material Category</th>
                              <th width="35%">Material Items</th>
                              <th>@ Price</th>
                              <th>Qty</th>
                              <th>Total Price</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>FLTRAC -- Filter AC</td>
                              <td>FLTRAC-1001 -- Filter AC LG Dual Cool</td>
                              <td>Rp. 550,000,-</td>
                              <td>1</td>
                              <td>Rp. 550,000,-</td>
                            </tr>
                          </tbody>
                        </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailMaintenance;
