import React, { Component } from 'react';
import { Card,  CardBody, Button, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailEquipment extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Equipment" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="12">
                    <Table responsive bordered>
                        <tbody>
                          <tr>
                            <th>Property Name</th>
                            <td>HURICANE BUILDING</td>
                          </tr>
                          <tr>
                            <th>Unit Code</th>
                            <td>A0101</td>
                          </tr>
                          <tr>
                            <th>Equipment Category</th>
                            <td>Air Conditioner</td>
                          </tr>
                          <tr>
                            <th>Brand</th>
                            <td>LG</td>
                          </tr>
                          <tr>
                            <th>Equipment Code</th>
                            <td>AC-10001</td>
                          </tr>
                          <tr>
                            <th>Equipment Name</th>
                            <td>LG DUALCOOL, 1.5PK</td>
                          </tr>
                          <tr>
                            <th>Model</th>
                            <td>P13RV3</td>
                          </tr>
                          <tr>
                            <th>Serial Number</th>
                            <td>P13RV3716231235</td>
                          </tr>
                          <tr>
                            <th>Purchase Price</th>
                            <td>Rp. 4,000,000,-</td>
                          </tr>
                          <tr>
                            <th>Sell Price</th>
                            <td>Rp. 3,000,000,-</td>
                          </tr>
                          <tr>
                            <th>Purchase Date</th>
                            <td>08/01/2019</td>
                          </tr>
                          <tr>
                            <th>Expired Date</th>
                            <td></td>
                          </tr>
                          <tr>
                            <th>Guaranted Date</th>
                            <td>07/31/2020</td>
                          </tr>
                          <tr>
                            <th>Inventory Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>COGS Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>Sales Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>Note</th>
                            <td>Test Note</td>
                          </tr>
                          <tr>
                            <th>Status</th>
                            <td>Active</td>
                          </tr>
                        </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailEquipment;
