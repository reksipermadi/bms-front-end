import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, equipmentCode: 'AC-10001', equipmentName: 'LG DUALCOOL, 1.5PK', equipmentCategory: 'Air Conditioner', brand: 'LG', propertyName: 'HURICANE BUILDING', unitCode: 'A0101', status: 'active' },
  { id: 1, equipmentCode: 'AC-10002', equipmentName: 'AC Dual Cool Eco', equipmentCategory: 'Air Conditioner', brand: 'LG', propertyName: 'HURICANE BUILDING', unitCode: 'A0101', status: 'active' },
  { id: 1, equipmentCode: 'AC-10003', equipmentName: 'AC Dual Cool Smart', equipmentCategory: 'Air Conditioner', brand: 'LG', propertyName: 'HURICANE BUILDING', unitCode: 'A0101', status: 'active' },
];
const columns = [
  {
    name: 'Equipment Code',
    selector: 'equipmentCode',
    sortable: true,
  },
  {
    name: 'Equipment Name',
    selector: 'equipmentName',
    sortable: true,
  },
  {
    name: 'Equipment Category',
    selector: 'equipmentCategory',
    sortable: true,
  },
  {
    name: 'Brand',
    selector: 'brand',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Unit Code',
    selector: 'unitCode',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Equipment extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Equipment" />
            <DataTable
              title="Data Equipment"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Equipment;
