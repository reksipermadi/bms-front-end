import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddEquipment extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Equipment" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Equipment</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="property">Property</Label>
                        <InputGroup>  
                          <Input type="select" name="property" id="property">
                            <option value="0">Choose Property</option>
                            <option value="1">HRC - Huricane Building</option>
                            <option value="2">HRC - Huricane Apartment</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="unit">Unit</Label>
                        <Input type="select" name="unit" id="unit">
                          <option value="0">Choose Unit</option>
                        </Input>
                        <FormText className="help-block">Please select unit</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="category">Equipment Category</Label>
                        <InputGroup>  
                          <Input type="select" name="category" id="category">
                            <option value="0">Choose Category</option>
                            <option value="1">EQP-AC -- Air Conditioner</option>
                            <option value="2">EQP-FRG -- Fridges</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select category</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="brand">Brand</Label>
                        <Input type="select" name="brand" id="brand">
                          <option value="0">Brand</option>
                          <option value="1">LG</option>
                        </Input>
                        <FormText className="help-block">Please select brand</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="equipmentCode">Equipment Code</Label>
                        <Input type="text" id="equipmentCode" name="equipmentCode" placeholder="Enter equipment code.." />
                        <FormText className="help-block">Please enter equipment code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="equipmentName">Equipment Name</Label>
                        <Input type="text" id="equipmentName" name="equipmentName" placeholder="Enter equipment name.." />
                        <FormText className="help-block">Please enter equipment name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="model">Model</Label>
                        <Input type="text" id="model" name="model" placeholder="Enter model.." />
                        <FormText className="help-block">Please enter model</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="serialNumber">Serial Number</Label>
                        <Input type="text" id="serialNumber" name="serialNumber" placeholder="Enter serial number.." />
                        <FormText className="help-block">Please enter serial number</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="purchasePrice">Purchase Price</Label>
                        <Input type="number" id="purchasePrice" name="purchasePrice" placeholder="Enter purchase price.." />
                        <FormText className="help-block">Please enter purchase price</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="sellPrice">Sell Price</Label>
                        <Input type="number" id="sellPrice" name="sellPrice" placeholder="Enter sell price.." />
                        <FormText className="help-block">Please enter sell price</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="purchaseDate">Purchase Date</Label>
                        <InputGroup>  
                          <Input type="date" id="purchaseDate" name="purchaseDate" placeholder="Enter purchase date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter purchase date</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="expiredDate">Expired Date</Label>
                        <InputGroup>  
                          <Input type="date" id="expiredDate" name="expiredDate" placeholder="Enter expired date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter expired date</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="guarantedDate">Guaranted Date</Label>
                        <InputGroup>  
                          <Input type="date" id="guarantedDate" name="guarantedDate" placeholder="Enter guaranted date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter guaranted date</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                        <Input type="select" name="account" id="account">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="cogsAccount">COGS Account</Label>
                        <Input type="select" name="cogsAccount" id="cogsAccount">
                          <option value="0">Choose COGS Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select cogs account</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="salesAccount">Sales Account</Label>
                        <Input type="select" name="salesAccount" id="salesAccount">
                          <option value="0">Choose Sales Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select sales account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="note">Note</Label>
                        <Input type="textarea" id="note" name="note" placeholder="Enter note.." />
                        <FormText className="help-block">Please enter note</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddEquipment;
