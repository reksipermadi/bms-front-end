import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddBrand extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Brands" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Brands</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="brandName">Brand Name</Label>
                        <Input type="text" id="brandName" name="brandName" placeholder="Enter brand name.." />
                        <FormText className="help-block">Please enter brand name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="companyName">Company Name</Label>
                        <Input type="text" id="companyName" name="companyName" placeholder="Enter company name.." />
                        <FormText className="help-block">Please enter company name</FormText>
                      </FormGroup>

                      <FormGroup>
                        <Label htmlFor="companyAddress">Company Address</Label>
                        <Input type="textarea" id="companyAddress" name="companyAddress" placeholder="Enter company address.." />
                        <FormText className="help-block">Please enter company address</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="contactPerson">Contact Person</Label>
                        <Input type="text" id="contactPerson" name="contactPerson" placeholder="Enter contact person.." />
                        <FormText className="help-block">Please enter contact person</FormText>
                      </FormGroup>

                      <FormGroup>
                        <Label htmlFor="phone">Phone</Label>
                        <Input type="number" id="phone" name="phone" placeholder="Enter phone.." />
                        <FormText className="help-block">Please enter phone</FormText>
                      </FormGroup>

                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="email" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddBrand;
