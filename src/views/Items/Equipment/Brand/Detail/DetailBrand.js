import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailBrand extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Brands" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Brand Name</Label>
                      <FormText className="help-block">LG</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <Label>Company Name</Label>
                      <FormText className="help-block">LG Indonesia</FormText>
                    </FormGroup>

                    <FormGroup>
                      <Label>Company Address</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Contact Person</Label>
                      <FormText className="help-block">Branch - Jakarta</FormText>
                    </FormGroup>

                    <FormGroup>
                      <Label>Phone</Label>
                      <FormText className="help-block">14010</FormText>
                    </FormGroup>

                    <FormGroup>
                      <Label>Email</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailBrand;
