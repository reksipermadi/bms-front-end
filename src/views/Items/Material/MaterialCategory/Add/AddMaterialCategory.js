import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddMaterialCategory extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Material Category" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Material Category</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="materialCategoryCode">Material Category Code</Label>
                        <Input type="text" id="materialCategoryCode" name="materialCategoryCode" placeholder="Enter material category code.." />
                        <FormText className="help-block">Please enter material category code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="materialCategoryName">Material Category Name</Label>
                        <Input type="text" id="materialCategoryName" name="materialCategoryName" placeholder="Enter material category name.." />
                        <FormText className="help-block">Please enter material category name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMaterialCategory;
