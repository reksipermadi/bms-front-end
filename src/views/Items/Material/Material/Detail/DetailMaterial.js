import React, { Component } from 'react';
import { Card,  CardBody, Button, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailMaterial extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Material" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="12">
                    <Table responsive bordered>
                        <tbody>
                          <tr>
                            <th>Material Category</th>
                            <td>Filter AC</td>
                          </tr>
                          <tr>
                            <th>Warehouse</th>
                            <td>Warehouse 1</td>
                          </tr>
                          <tr>
                            <th>Material Code</th>
                            <td>FLTRAC-1001</td>
                          </tr>
                          <tr>
                            <th>Material Name</th>
                            <td>Filter AC LG Dual Cool</td>
                          </tr>
                          <tr>
                            <th>Purchase Price</th>
                            <td>Rp. 500,000,-</td>
                          </tr>
                          <tr>
                            <th>Sell Price</th>
                            <td>Rp. 550,000,-</td>
                          </tr>
                          <tr>
                            <th>Purchase Date</th>
                            <td>08/01/2019</td>
                          </tr>
                          <tr>
                            <th>Guaranted Date</th>
                            <td>08/01/2020</td>
                          </tr>
                          <tr>
                            <th>Material Stock</th>
                            <td>10</td>
                          </tr>
                          <tr>
                            <th>Inventory Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>COGS Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>Sales Account</th>
                            <td>1.01.01.01.02.01 -- Kas Kecil</td>
                          </tr>
                          <tr>
                            <th>Note</th>
                            <td>Test Note</td>
                          </tr>
                          <tr>
                            <th>Status</th>
                            <td>Active</td>
                          </tr>
                        </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailMaterial;
