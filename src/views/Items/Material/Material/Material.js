import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, materialCode: 'FLTRAC-1001', materialName: 'Filter AC LG Dual Cool', materialCategory: 'Filter AC', warehouse: 'Warehouse 1', sellPrice: 'Rp. 550,000,-', stock: '10', status: 'active' },
];
const columns = [
  {
    name: 'Material Code',
    selector: 'materialCode',
    sortable: true,
  },
  {
    name: 'Material Name',
    selector: 'materialName',
    sortable: true,
  },
  {
    name: 'Material Category',
    selector: 'materialCategory',
    sortable: true,
  },
  {
    name: 'Warehouse',
    selector: 'warehouse',
    sortable: true,
  },
  {
    name: 'Sell Price',
    selector: 'sellPrice',
    sortable: true,
  },
  {
    name: 'Stock',
    selector: 'stock',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Material extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Material" />
            <DataTable
              title="Data Material"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Material;
