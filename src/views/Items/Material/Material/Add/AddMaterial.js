import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddMaterial extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Material" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Material</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="materialCategory">Material Category</Label>
                        <InputGroup>  
                          <Input type="select" name="materialCategory" id="materialCategory">
                            <option value="0">Choose Category</option>
                            <option value="1">BAUT-05 -- Baut 0,5 inchi</option>
                            <option value="2">FLTRAC -- Filter AC</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select material category</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="warehouse">Warehouse</Label>
                        <Input type="select" name="warehouse" id="warehouse">
                          <option value="0">Choose Warehouse</option>
                          <option value="1">WH0001 -- Warehouse 1</option>
                        </Input>
                        <FormText className="help-block">Please select warehouse</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="materialCode">Material Code</Label>
                        <Input type="text" id="materialCode" name="materialCode" placeholder="Enter material code.." />
                        <FormText className="help-block">Please enter material code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="materialName">Material Name</Label>
                        <Input type="text" id="materialName" name="materialName" placeholder="Enter material name.." />
                        <FormText className="help-block">Please enter material name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="purchasePrice">Purchase Price</Label>
                        <Input type="number" id="purchasePrice" name="purchasePrice" placeholder="Enter purchase price.." />
                        <FormText className="help-block">Please enter purchase price</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="sellPrice">Sell Price</Label>
                        <Input type="number" id="sellPrice" name="sellPrice" placeholder="Enter sell price.." />
                        <FormText className="help-block">Please enter sell price</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="purchaseDate">Purchase Date</Label>
                        <InputGroup>  
                          <Input type="date" id="purchaseDate" name="purchaseDate" placeholder="Enter purchase date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter purchase date</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="guarantedDate">Guaranted Date</Label>
                        <InputGroup>  
                          <Input type="date" id="guarantedDate" name="guarantedDate" placeholder="Enter guaranted date.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter guaranted date</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="stock">Stock</Label>
                        <Input type="number" id="stock" name="stock" placeholder="Enter stock.." />
                        <FormText className="help-block">Please enter stock</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="inventoryAccount">Inventory Account</Label>
                        <Input type="select" name="inventoryAccount" id="inventoryAccount">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select inventory account</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="cogsAccount">COGS Account</Label>
                        <Input type="select" name="cogsAccount" id="cogsAccount">
                          <option value="0">Choose COGS Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select cogs account</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="salesAccount">Sales Account</Label>
                        <Input type="select" name="salesAccount" id="salesAccount">
                          <option value="0">Choose Sales Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select sales account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="note">Note</Label>
                        <Input type="textarea" id="note" name="note" placeholder="Enter note.." />
                        <FormText className="help-block">Please enter note</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMaterial;
