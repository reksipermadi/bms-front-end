import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddInvoiceCategory extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Invoice Category" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Invoice Category</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="invoiceGroup">Invoice Group</Label>
                        <Input type="select" name="invoiceGroup" id="invoiceGroup">
                          <option value="0">Choose Invoice Group</option>
                          <option value="1">Money In -- Deposit</option>
                          <option value="2">Money In -- Invoice</option>
                          <option value="3">Money In -- Payment</option>
                        </Input>
                        <FormText className="help-block">Please select invoice group</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="categoryName">Category Name</Label>
                        <Input type="text" id="categoryName" name="categoryName" placeholder="Enter category name.." />
                        <FormText className="help-block">Please enter category name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <h3 className="text-info">Account</h3>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th>Account</th>
                        <th className="text-center">Debet</th>
                        <th className="text-center">Credit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <Input type="select" name="account" id="account">
                            <option value="0">Choose Account</option>
                            <option value="1">1 -- AKTIVA</option>
                            <option value="2">1.01 -- Aset Lancar</option>
                            <option value="3">1.01.01 -- Kas dan Setara Kas</option>
                          </Input>
                        </td>
                        <td className="text-center">
                          <Input className="form-check-input" type="checkbox" id="debit" name="debit" />
                        </td>
                        <td className="text-center">
                          <Input className="form-check-input" type="checkbox" id="kredit" name="kredit" />
                        </td>
                        <td>
                          <Button color="danger"><i class="fa fa-trash"></i></Button>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                  <hr/>
                  <Button color="success">
                    <i class="fa fa-plus fa-lg"></i> Account
                  </Button>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddInvoiceCategory;
