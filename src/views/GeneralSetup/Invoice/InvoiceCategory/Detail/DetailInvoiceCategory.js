import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailInvoiceCategory extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Invoice Category" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Category Name</Label>
                      <FormText className="help-block">AR/ Rent Building</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Group Name</Label>
                      <FormText className="help-block">Invoice</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Group Type</Label>
                      <FormText className="help-block">Money In</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th>Account</th>
                      <th>Journal Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>4.01.02.01 -- Sewa</td>
                      <td>Debet</td>
                    </tr>
                    <tr>
                      <td>2.04.02.01 -- Pendapatan Ditangguhkan - Unit Apartemen</td>
                      <td>Credit</td>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailInvoiceCategory;
