import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, categoryName: 'AR/ Rent Building', groupName: 'Invoice', groupType: 'Money In' },
  { id: 2, categoryName: 'AR/ Service Charge', groupName: 'Invoice', groupType: 'Money In' },
  { id: 3, categoryName: 'Deposit Infront', groupName: 'Deposit', groupType: 'Money In' },
  { id: 4, categoryName: 'Ppn Keluaran', groupName: 'Invoice', groupType: 'Money In' },
  { id: 5, categoryName: 'Security Deposit', groupName: 'Deposit', groupType: 'Money In' },
];
const columns = [
  {
    name: 'Category Name',
    selector: 'categoryName',
    sortable: true,
  },
  {
    name: 'Group Name',
    selector: 'groupName',
    sortable: true,
  },
  {
    name: 'Group Type',
    selector: 'groupType',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class InvoiceCategory extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Invoice Category" />
        <DataTable
          title="Data Invoice Category"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default InvoiceCategory;
