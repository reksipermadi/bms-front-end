import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, invoiceGroupName: 'Deposit', invoiceGroupType: 'Money In', account: '1.01.01 -- Kas dan Setara Kas'},
  { id: 2, invoiceGroupName: 'Invoice', invoiceGroupType: 'Money In', account: '1.01.03 -- Piutang'},
  { id: 3, invoiceGroupName: 'Payment', invoiceGroupType: 'Money In', account: '1.01.01.02 -- Bank'},
];
const columns = [
  {
    name: 'Invoice Group Name',
    selector: 'invoiceGroupName',
    sortable: true,
  },
  {
    name: 'Invoice Group Type',
    selector: 'invoiceGroupType',
    sortable: true,
  },
  {
    name: 'Account',
    selector: 'account',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class InvoiceGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Invoice Group" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Invoice Group"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default InvoiceGroup;
