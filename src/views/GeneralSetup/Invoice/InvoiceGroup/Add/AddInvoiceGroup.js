import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddInvoiceGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Invoice Group" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Invoice Group</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="groupType">Group Type</Label>
                        <Input type="select" name="groupType" id="groupType">
                          <option value="0">Choose Group Type</option>
                          <option value="1">Money In</option>
                          <option value="2">Money Out</option>
                        </Input>
                        <FormText className="help-block">Please select group type</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="invoiceGroupName">Invoice Group Name</Label>
                        <Input type="text" id="invoiceGroupName" name="invoiceGroupName" placeholder="Enter invoice group name.." />
                        <FormText className="help-block">Please enter inovice group name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                        <Input type="select" name="account" id="account">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddInvoiceGroup;
