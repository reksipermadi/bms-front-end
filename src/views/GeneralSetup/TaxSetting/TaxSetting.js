import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, code: 'Pph10', name: 'Pph 10%', type: 'Pph', rate: '10.00' },
  { id: 2, code: 'Ppn10', name: 'Ppn 10%', type: 'Ppn', rate: '10.00' },
];
const columns = [
  {
    name: 'Code',
    selector: 'code',
    sortable: true,
  },
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Type',
    selector: 'type',
    sortable: true,
  },
  {
    name: 'Rate (%)',
    selector: 'rate',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class TaxSetting extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Tax Setting" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Tax Setting"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default TaxSetting;
