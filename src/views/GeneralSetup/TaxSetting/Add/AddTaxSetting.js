import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddTaxSetting extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Tax Setting" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Tax Setting</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxType">Tax Type</Label>
                          <Input type="select" name="taxType" id="taxType">
                            <option value="0">Choose Tax Type</option>
                            <option value="1">Pph</option>
                            <option value="2">Ppn</option>
                          </Input>
                        <FormText className="help-block">Please select tax type</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                          <Input type="select" name="account" id="account">
                            <option value="0">Choose Account</option>
                            <option value="1">1 - AKTIVA</option>
                            <option value="2">1.01 -- Aset Lancar</option>
                          </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxCode">Tax Code</Label>
                        <Input type="text" id="taxCode" name="taxCode" placeholder="Enter tax code.." />
                        <FormText className="help-block">Please enter tax code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxName">Tax Name</Label>
                        <Input type="text" id="taxName" name="taxName" placeholder="Enter tax name.." />
                        <FormText className="help-block">Please enter tax name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxRate">Tax Rate (%)</Label>
                        <Input type="text" id="taxRate" name="taxRate" placeholder="Enter tax rate.." />
                        <FormText className="help-block">Please enter tax rate</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddTaxSetting;
