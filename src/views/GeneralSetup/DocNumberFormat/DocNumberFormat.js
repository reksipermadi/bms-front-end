import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, code: 'InvoicePayment', description: 'Invoice Payment', category: 'BM', numberFormat: 'CATEGORY/N/mm/y', sequence: 'Monthly', lastNumber: '19' },
  { id: 1, code: 'Maintenance', description: 'Request Maintenance', category: 'MNT', numberFormat: 'CATEGORY/N/mm/y', sequence: 'Monthly', lastNumber: '1' },
  { id: 1, code: 'UnitRentMoveIn', description: 'Unit Rent - Move In', category: 'INV', numberFormat: 'CATEGORY/N/mm/y', sequence: 'Monthly', lastNumber: '25' },
];
const columns = [
  {
    name: 'Code',
    selector: 'code',
    sortable: true,
  },
  {
    name: 'Description',
    selector: 'description',
    sortable: true,
  },
  {
    name: 'Category',
    selector: 'category',
    sortable: true,
  },
  {
    name: 'Number Format',
    selector: 'numberFormat',
    sortable: true,
  },
  {
    name: 'Sequence',
    selector: 'sequence',
    sortable: true,
  },
  {
    name: 'Last Number',
    selector: 'lastNumber',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class DocNumberFormat extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Doc Number Format" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Doc Number Format"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default DocNumberFormat;
