import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddDocNumberFormat extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Doc Number Format" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Doc Number Format</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="code">Code</Label>
                        <Input type="text" id="code" name="code" placeholder="Enter code.." />
                        <FormText className="help-block">Please enter code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="text" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="categoryCode">Category Code</Label>
                        <Input type="text" id="categoryCode" name="categoryCode" placeholder="Enter category code.." />
                        <FormText className="help-block">Please enter category code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="sequence">Sequence</Label>
                          <Input type="select" name="sequence" id="sequence">
                            <option value="0">Choose Sequence</option>
                            <option value="1">Daily</option>
                            <option value="2">Monthly</option>
                            <option value="3">Yearly</option>
                          </Input>
                        <FormText className="help-block">Please select sequence</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="numberLength">Number Length</Label>
                        <Input type="text" id="numberLength" name="numberLength" placeholder="Enter number length.." />
                        <FormText className="help-block">Please enter number length</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="12">
                      <Label className="no-margin">Number Format</Label>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="2">
                      <div className="form-inline">
                        <FormGroup>
                          <Input type="select" name="noFormat[0]" id="noFormat[0]">
                            <option value="0">Choose</option>
                            <option value="1">Number</option>
                            <option value="2">Category Code</option>
                            <option value="3">Property ID</option>
                            <option value="4">Unit Code</option>
                            <option value="5">Month (1)</option>
                            <option value="6">Month (01)</option>
                            <option value="7">Month (I)</option>
                          </Input>
                          <span className="bms-separator-number">/</span>
                        </FormGroup>
                      </div>
                    </Col>
                    <Col md="2">
                      <div className="form-inline">
                        <FormGroup>
                          <Input type="select" name="noFormat[1]" id="noFormat[1]">
                            <option value="0">Choose</option>
                            <option value="1">Number</option>
                            <option value="2">Category Code</option>
                            <option value="3">Property ID</option>
                            <option value="4">Unit Code</option>
                            <option value="5">Month (1)</option>
                            <option value="6">Month (01)</option>
                            <option value="7">Month (I)</option>
                          </Input>
                          <span className="bms-separator-number">/</span>
                        </FormGroup>
                      </div>
                    </Col>
                    <Col md="2">
                      <div className="form-inline">
                        <FormGroup>
                          <Input type="select" name="noFormat[2]" id="noFormat[2]">
                            <option value="0">Choose</option>
                            <option value="1">Number</option>
                            <option value="2">Category Code</option>
                            <option value="3">Property ID</option>
                            <option value="4">Unit Code</option>
                            <option value="5">Month (1)</option>
                            <option value="6">Month (01)</option>
                            <option value="7">Month (I)</option>
                          </Input>
                          <span className="bms-separator-number">/</span>
                        </FormGroup>
                      </div>
                    </Col>
                    <Col md="2">
                      <div className="form-inline">
                        <FormGroup>
                          <Input type="select" name="noFormat[3]" id="noFormat[3]">
                            <option value="0">Choose</option>
                            <option value="1">Number</option>
                            <option value="2">Category Code</option>
                            <option value="3">Property ID</option>
                            <option value="4">Unit Code</option>
                            <option value="5">Month (1)</option>
                            <option value="6">Month (01)</option>
                            <option value="7">Month (I)</option>
                          </Input>
                          <span className="bms-separator-number">/</span>
                        </FormGroup>
                      </div>
                    </Col>
                    <Col md="2">
                      <div className="form-inline">
                          <FormGroup>
                            <Input type="select" name="noFormat[3]" id="noFormat[3]">
                              <option value="0">Choose</option>
                              <option value="1">Number</option>
                              <option value="2">Category Code</option>
                              <option value="3">Property ID</option>
                              <option value="4">Unit Code</option>
                              <option value="5">Month (1)</option>
                              <option value="6">Month (01)</option>
                              <option value="7">Month (I)</option>
                            </Input>
                          </FormGroup>
                        </div>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddDocNumberFormat;
