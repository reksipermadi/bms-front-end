import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, warehouseCode: 'WH0001', warehouseName: 'Warehouse 1', propertyName: 'HURICANE BUILDING'}
];
const columns = [
  {
    name: 'Warehouse Code',
    selector: 'warehouseCode',
    sortable: true,
  },
  {
    name: 'Warehouse Name',
    selector: 'warehouseName',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Warehouse extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Warehouse" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Warehouse"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default Warehouse;
