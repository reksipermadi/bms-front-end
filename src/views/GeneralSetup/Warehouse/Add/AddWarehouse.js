import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddWarehouse extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Warehouse" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Warehouse</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="property">Property</Label>
                          <Input type="select" name="property" id="property">
                            <option value="0">Choose Property</option>
                            <option value="1">HRC - HURICANE BUILDING</option>
                            <option value="2">HRCA - HURICANE APARTMENT</option>
                            <option value="3">HRCB - HURICANE APARTMENT</option>
                          </Input>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="warehouseCode">Warehouse Code</Label>
                        <Input type="text" id="warehouseCode" name="warehouseCode" placeholder="Enter warehouse code.." />
                        <FormText className="help-block">Please enter warehouse code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="warehouseName">Warehouse Name</Label>
                        <Input type="text" id="warehouseName" name="warehouseName" placeholder="Enter warehouse name.." />
                        <FormText className="help-block">Please enter warehouse name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddWarehouse;
