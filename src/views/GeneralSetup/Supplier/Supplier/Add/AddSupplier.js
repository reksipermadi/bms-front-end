import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddSupplier extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Supplier" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Supplier</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="supplierGroup">Supplier Group</Label>
                        <InputGroup>  
                          <Input type="select" name="supplierGroup" id="supplierGroup">
                            <option value="0">Choose Suplier Group</option>
                            <option value="1">Supplier Group 1</option>
                            <option value="2">Supplier Group 2</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select supplier group</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="supplierType">Supplier Type</Label>
                        <Input type="select" name="supplierType" id="supplierType">
                          <option value="0">Choose Supplier Type</option>
                          <option value="1">Personal</option>
                          <option value="2">PT</option>
                        </Input>
                        <FormText className="help-block">Please select supplier type</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="supplierCode">Supplier Code</Label>
                        <Input type="text" id="supplierCode" name="supplierCode" placeholder="Enter supplier code.." />
                        <FormText className="help-block">Please enter supplier code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="supplierName">Supplier Name</Label>
                        <Input type="text" id="supplierName" name="supplierName" placeholder="Enter supplier name.." />
                        <FormText className="help-block">Please enter supplier name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="contactPersonName">Contact Person Name</Label>
                        <Input type="text" id="contactPersonName" name="contactPersonName" placeholder="Enter contact person name.." />
                        <FormText className="help-block">Please enter contact person name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxIdNumber">Tax ID Number</Label>
                        <Input type="number" id="taxIdNumber" name="taxIdNumber" placeholder="Enter tax id number.." />
                        <FormText className="help-block">Please enter tax id number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="mobileNumber">Mobile Number</Label>
                        <Input type="number" id="mobileNumber" name="mobileNumber" placeholder="Enter mobile number.." />
                        <FormText className="help-block">Please enter mobile number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="email" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="phoneNumber">Phone Number</Label>
                        <Input type="number" id="phoneNumber" name="phoneNumber" placeholder="Enter phone number.." />
                        <FormText className="help-block">Please enter phone number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="faxNumber">Fax Number</Label>
                        <Input type="number" id="faxNumber" name="faxNumber" placeholder="Enter fax number.." />
                        <FormText className="help-block">Please enter fax number</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label htmlFor="address">Address</Label>
                        <Input type="textarea" id="address" name="address" placeholder="Enter address.." />
                        <FormText className="help-block">Please enter address</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="country">Country</Label>
                        <Input type="select" name="country" id="country">
                          <option value="0">Choose Country</option>
                          <option value="1">ID -- Indonesia</option>
                        </Input>
                        <FormText className="help-block">Please select country</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="province">Province</Label>
                        <Input type="select" name="province" id="province">
                          <option value="0">Choose Province</option>
                        </Input>
                        <FormText className="help-block">Please select province</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="city">City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Choose City</option>
                        </Input>
                        <FormText className="help-block">Please select city</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="postalCode">Postal Code</Label>
                        <Input type="number" id="postalCode" name="postalCode" placeholder="Enter postal code.." />
                        <FormText className="help-block">Please enter postal code</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddSupplier;
