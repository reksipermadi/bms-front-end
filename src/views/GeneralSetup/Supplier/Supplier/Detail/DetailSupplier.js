import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailSupplier extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Supplier" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Supplier Group</Label>
                      <FormText className="help-block">Supplier Group 1</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Supplier Type</Label>
                      <FormText className="help-block">PT</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Account</Label>
                      <FormText className="help-block">1.01.01.01.02.01 -- Kas Kecil</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Supplier Code</Label>
                      <FormText className="help-block">SUP0001</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Supplier Name</Label>
                      <FormText className="help-block">Supplier 1</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Contact Person Name</Label>
                      <FormText className="help-block">Joni</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Tax ID Number</Label>
                      <FormText className="help-block">15426123876</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Mobile Number</Label>
                      <FormText className="help-block">0898128318612</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Email</Label>
                      <FormText className="help-block">sup1@mail.com</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Phone Number</Label>
                      <FormText className="help-block">021 5432167</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Fax Number</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12">
                    <FormGroup>
                      <Label>Address</Label>
                      <FormText className="help-block">Jl. Tanah Tinggi No. 301</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Country</Label>
                      <FormText className="help-block">Indonesia</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Province</Label>
                      <FormText className="help-block">DKI Jakarta</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>City</Label>
                      <FormText className="help-block">Jakarta Timur</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Postal Code</Label>
                      <FormText className="help-block">13543</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Status</Label>
                      <FormText className="help-block">Active</FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailSupplier;
