import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, supplierCode: 'SUP0001', supplierName: 'Supplier 1', supplierGroup: 'Supplier Group 1', contactPerson: 'Joni', phone: '021 5432167', email: 'sup1@mail.com', address: 'Jl. Tanah Tinggi No. 301', city: 'Jakarta Timur'},
];
const columns = [
  {
    name: 'Supplier Code',
    selector: 'supplierCode',
    sortable: true,
  },
  {
    name: 'Supplier Name',
    selector: 'supplierName',
    sortable: true,
  },
  {
    name: 'Supplier Group',
    selector: 'supplierGroup',
    sortable: true,
  },
  {
    name: 'Contact Person',
    selector: 'contactPerson',
    sortable: true,
  },
  {
    name: 'Phone',
    selector: 'phone',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'City',
    selector: 'city',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Supplier extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Supplier" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Supplier"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
            <br/>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Supplier;
