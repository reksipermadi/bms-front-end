import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddChartOfAccount extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Chart of Account" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Chart of Account</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="12">
                    <FormGroup>
                      <Label htmlFor="accountParent">Account Parent</Label>
                      <Input type="select" name="accountParent" id="accountParent">
                        <option value="1">1 -- AKTIVA</option>
                        <option value="2">1.01 -- Aset Lancar</option>
                        <option value="3">1.01.01 -- Kas dan Setara Kas</option>
                      </Input>
                      <FormText className="help-block">Please select Account Parent</FormText>
                    </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="accountNo">Account No</Label>
                        <Input type="text" id="accountNo" name="accountNo" placeholder="Enter Account No.." />
                        <FormText className="help-block">Please enter account no</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="accountName">Account Name</Label>
                        <Input type="text" id="accountName" name="accountName" placeholder="Enter Account Name.." />
                        <FormText className="help-block">Please enter account name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="accountType">Account Type</Label>
                        <Input type="select" name="accountType" id="accountType">
                          <option value="1">Debit</option>
                          <option value="2">Kredit</option>
                        </Input>
                        <FormText className="help-block">Please select account type</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddChartOfAccount;
