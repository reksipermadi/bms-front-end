import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, accountNo: '1', accountName: 'AKTIVA', accountType: 'Debit' },
  { id: 2, accountNo: '1.01', accountName: 'Aset Lancar', accountType: 'Kredit' },
  { id: 3, accountNo: '1.01.01', accountName: 'Kas dan Setara Kas', accountType: 'Debit' },
  { id: 4, accountNo: '1.01.01.01', accountName: 'Kas', accountType: 'Kredit' },
  { id: 5, accountNo: '1.01.01.01.01', accountName: 'Kas Besar', accountType: 'Debit' },
];
const columns = [
  {
    name: 'Account No',
    selector: 'accountNo',
    sortable: true,
  },
  {
    name: 'Account Name',
    selector: 'accountName',
    sortable: true,
  },
  {
    name: 'Account Type',
    selector: 'accountType',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class ChartOfAccount extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Chart of Account" />
        <DataTable
          title="Data Chart of Account"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default ChartOfAccount;
