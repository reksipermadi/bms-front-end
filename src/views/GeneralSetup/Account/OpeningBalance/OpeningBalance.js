import React, { Component } from 'react';
import { Input, Button, Card, CardBody, CardFooter } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, accountNo: '1.01.01', accountName: 'Kas Besar', debit: '1000000', credit: '0' },
  { id: 2, accountNo: '1.02.02', accountName: 'Pengeluaran', debit: '100', credit: '1000000' },
];
const columns = [
  {
    name: 'Account No',
    selector: 'accountNo',
    sortable: true,
  },
  {
    name: 'Account Name',
    selector: 'accountName',
    sortable: true,
  },
  {
    name: 'Debit',
    selector: 'debit',
    sortable: true,
  },
  {
    name: 'Credit',
    selector: 'credit',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button outline className="mr-2" color="success">
      <i class="fa fa-dollar fa-lg"></i> Saldo Awal : 0
    </Button>
    <Button outline className="mr-2" color="primary">
      <i class="fa fa-check fa-lg"></i> Unbalanced
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class OpeningBalance extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Opening Balance" />
            <DataTable
              title="Data Opening Balance"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
          <CardFooter className="text-right">
            <span><i class="fa fa-dollar"></i> Debit : 1000100&nbsp; &nbsp;<i class="fa fa-dollar"></i> Credit : 1000000</span>
          </CardFooter>
        </Card>
      </div>
    );
  }
}

export default OpeningBalance;
