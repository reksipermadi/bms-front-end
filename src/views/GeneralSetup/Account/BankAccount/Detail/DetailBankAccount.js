import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailBankAccount extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Bank Account" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Group Type</Label>
                      <FormText className="help-block">Money In</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Property</Label>
                      <FormText className="help-block">HURICANE APARTMENT</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Bank Code</Label>
                      <FormText className="help-block">BKP</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Bank Name</Label>
                      <FormText className="help-block">BUKOPIN</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Bank Branch</Label>
                      <FormText className="help-block">Cawang</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Bank Account No</Label>
                      <FormText className="help-block">09023908234598</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Account</Label>
                      <FormText className="help-block">-- 1.01.01.02.02.01 -- Bank Operasional-Bukopin A/C: 1023759019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <Label>Remark</Label>
                      <FormText className="help-block">Uang Masuk</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Status</Label>
                      <FormText className="help-block">Active</FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailBankAccount;
