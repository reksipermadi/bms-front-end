import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, bankCode: 'BKP', bankName: 'Bukopin', bankBranch: 'Cawang', bankAccount: '09928387283', account: '-- 1.01.01.02.02.01 -- Bank Operasional-Bukopin A/C: 1023759019', propertyName: 'HURICANE APARTMENT', status: 'active' },
  { id: 2, bankCode: 'MDR-01', bankName: 'Mandiri', bankBranch: 'Lenteng Agung', bankAccount: '9.0.000.43.5', account: '	-- 1.01.01.01.02 -- Kas Kecil', propertyName: 'HURICANE BUILDING', status: 'active' },
  { id: 1, bankCode: 'BKP', bankName: 'Bukopin', bankBranch: 'Cawang', bankAccount: '09928387283', account: '-- 1.01.01.02.02.01 -- Bank Operasional-Bukopin A/C: 1023759019', propertyName: 'HURICANE APARTMENT', status: 'active' },
  { id: 2, bankCode: 'MDR-01', bankName: 'Mandiri', bankBranch: 'Lenteng Agung', bankAccount: '9.0.000.43.5', account: '	-- 1.01.01.01.02 -- Kas Kecil', propertyName: 'HURICANE BUILDING', status: 'active' },
  { id: 1, bankCode: 'BKP', bankName: 'Bukopin', bankBranch: 'Cawang', bankAccount: '09928387283', account: '-- 1.01.01.02.02.01 -- Bank Operasional-Bukopin A/C: 1023759019', propertyName: 'HURICANE APARTMENT', status: 'active' },
  { id: 2, bankCode: 'MDR-01', bankName: 'Mandiri', bankBranch: 'Lenteng Agung', bankAccount: '9.0.000.43.5', account: '	-- 1.01.01.01.02 -- Kas Kecil', propertyName: 'HURICANE BUILDING', status: 'active' },
];
const columns = [
  {
    name: 'Bank Code',
    selector: 'bankCode',
    sortable: true,
  },
  {
    name: 'Bank Name',
    selector: 'bankName',
    sortable: true,
  },
  {
    name: 'Bank Branch',
    selector: 'bankBranch',
    sortable: true,
  },
  {
    name: 'Bank Account',
    selector: 'bankAccount',
    sortable: true,
  },
  {
    name: 'Account',
    selector: 'account',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class BankAccount extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Bank Account" />
        <Card>
          <CardBody>
            <DataTable
              title="Bank Account"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default BankAccount;
