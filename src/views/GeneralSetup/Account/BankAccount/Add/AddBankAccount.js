import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddBankAccount extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Bank Account" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Bank Account</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="groupType">Group Type</Label>
                        <Input type="select" name="groupType" id="groupType">
                          <option value="0">Choose Group Type</option>
                          <option value="1">Money In</option>
                          <option value="2">Money Out</option>
                        </Input>
                        <FormText className="help-block">Please select group type</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="groupType">Property</Label>
                        <InputGroup>  
                          <Input type="select" name="property" id="property">
                            <option value="0">Choose Property</option>
                            <option value="1">HRC - Huricane Building</option>
                            <option value="2">HRC - Huricane Apartment</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="bankCode">Bank Code</Label>
                        <Input type="text" id="bankCode" name="bankCode" placeholder="Enter Bank Code.." />
                        <FormText className="help-block">Please enter bank code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="bankName">Bank Name</Label>
                        <Input type="text" id="bankName" name="bankName" placeholder="Enter Bank Name.." />
                        <FormText className="help-block">Please enter bank name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="bankBranch">Bank Branch</Label>
                        <Input type="text" id="bankBranch" name="bankBranch" placeholder="Enter Bank Branch.." />
                        <FormText className="help-block">Please enter bank branch</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="bankAccountNo">Bank Account No</Label>
                        <Input type="text" id="bankAccountNo" name="bankAccountNo" placeholder="Enter Bank Account No.." />
                        <FormText className="help-block">Please enter bank account no</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                        <Input type="select" name="account" id="account">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="remark">Remark</Label>
                        <Input type="text" id="remark" name="remark" placeholder="Enter Remark.." />
                        <FormText className="help-block">Please enter remark</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddBankAccount;
