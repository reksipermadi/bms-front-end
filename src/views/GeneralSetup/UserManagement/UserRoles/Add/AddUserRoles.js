import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddUserRoles extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="User Roles" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add User Roles</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="roleName">Role Name</Label>
                        <Input type="text" id="roleName" name="roleName" placeholder="Enter role name.." />
                        <FormText className="help-block">Please enter role name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      <Table responsive bordered>
                        <thead>
                          <tr>
                            <td width="50"></td>
                            <td><b>Menu Name</b></td>
                            <td width="50"><b>Show</b></td>
                            <td width="50"><b>Create</b></td>
                            <td width="50"><b>Edit</b></td>
                            <td width="50"><b>Delete</b></td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Settings</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="appConfig" name="appConfig" /></td>
                            <td>App Config</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="appConfigShow" name="appConfigShow" disabled /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="appConfigCreate" name="appConfigCreate" disabled /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="appConfigEdit" name="appConfigEdit" disabled /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="appConfigDelete" name="appConfigDelete" disabled /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="masterData" name="masterData"  /></td>
                            <td>Master Data</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="masterDataShow" name="masterDataShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="masterDataCreate" name="masterDataCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="masterDataEdit" name="masterDataEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="masterDataDelete" name="masterDataDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>General Setup</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="account" name="account"  /></td>
                            <td>Account</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="accountShow" name="accountShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="accountCreate" name="accountCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="accountEdit" name="accountEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="accountDelete" name="accountDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="budget" name="budget"  /></td>
                            <td>Budget</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="budgetShow" name="budgetShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="budgetCreate" name="budgetCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="budgetEdit" name="budgetEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="budgetDelete" name="budgetDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="complaints" name="complaints"  /></td>
                            <td>Complaints</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="complaintsShow" name="complaintsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="complaintsCreate" name="complaintsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="complaintsEdit" name="complaintsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="complaintsDelete" name="complaintsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="organizations" name="organizations"  /></td>
                            <td>Organizations</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="organizationsShow" name="organizationsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="organizationsCreate" name="organizationsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="organizationsEdit" name="organizationsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="organizationsDelete" name="organizationsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoices" name="invoices"  /></td>
                            <td>Invoices</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesShow" name="invoicesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesCreate" name="invoicesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesEdit" name="invoicesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesDelete" name="invoicesDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="meters" name="meters"  /></td>
                            <td>Meters</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="metersShow" name="metersShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="metersCreate" name="metersCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="metersEdit" name="metersEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="metersDelete" name="metersDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="services" name="services"  /></td>
                            <td>Services</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="servicesShow" name="servicesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="servicesCreate" name="servicesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="servicesEdit" name="servicesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="servicesDelete" name="servicesDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="suppliers" name="suppliers"  /></td>
                            <td>Suppliers</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="suppliersShow" name="suppliersShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="suppliersCreate" name="suppliersCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="suppliersEdit" name="suppliersEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="suppliersDelete" name="suppliersDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="userManagement" name="userManagement"  /></td>
                            <td>User Management</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="userManagementShow" name="userManagementShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="userManagementCreate" name="userManagementCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="userManagementEdit" name="userManagementEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="userManagementDelete" name="userManagementDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="docNumberFormat" name="docNumberFormat"  /></td>
                            <td>Doc Number Format</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="docNumberFormatShow" name="docNumberFormatShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="docNumberFormatCreate" name="docNumberFormatCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="docNumberFormatEdit" name="docNumberFormatEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="docNumberFormatDelete" name="docNumberFormatDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="taxSettings" name="taxSettings"  /></td>
                            <td>Tax Settings</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="taxSettingsShow" name="taxSettingsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="taxSettingsCreate" name="taxSettingsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="taxSettingsEdit" name="taxSettingsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="taxSettingsDelete" name="taxSettingsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="warehouse" name="warehouse"  /></td>
                            <td>Warehouse</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="warehouseShow" name="warehouseShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="warehouseCreate" name="warehouseCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="warehouseEdit" name="warehouseEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="warehouseDelete" name="warehouseDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="leasableArea" name="leasableArea"  /></td>
                            <td>Leasable Area</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="leasableAreaShow" name="leasableAreaShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="leasableAreaCreate" name="leasableAreaCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="leasableAreaEdit" name="leasableAreaEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="leasableAreaDelete" name="leasableAreaDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Items</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="equipments" name="equipments"  /></td>
                            <td>Equipments</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="equipmentsShow" name="equipmentsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="equipmentsCreate" name="equipmentsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="equipmentsEdit" name="equipmentsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="equipmentsDelete" name="equipmentsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="materials" name="materials"  /></td>
                            <td>Materials</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="materialsShow" name="materialsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="materialsCreate" name="materialsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="materialsEdit" name="materialsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="materialsDelete" name="materialsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Profile</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companies" name="companies"  /></td>
                            <td>Companies</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesShow" name="companiesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesCreate" name="companiesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesEdit" name="companiesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesDelete" name="companiesDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesProperties" name="companiesProperties"  /></td>
                            <td>Companies Properties</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesPropertiesShow" name="companiesPropertiesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesPropertiesCreate" name="companiesPropertiesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesPropertiesEdit" name="companiesPropertiesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesPropertiesDelete" name="companiesPropertiesDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesUnits" name="companiesUnits"  /></td>
                            <td>Companies Units</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesUnitsShow" name="companiesUnitsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesUnitsCreate" name="companiesUnitsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesUnitsEdit" name="companiesUnitsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="companiesUnitsDelete" name="companiesUnitsDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="ownerTenant" name="ownerTenant"  /></td>
                            <td>Owner & Tenant</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="ownerTenantShow" name="ownerTenantShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="ownerTenantCreate" name="ownerTenantCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="ownerTenantEdit" name="ownerTenantEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="ownerTenantDelete" name="ownerTenantDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Tenancy</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="unitRent" name="unitRent"  /></td>
                            <td>Unit Rent</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="unitRentShow" name="unitRentShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="unitRentCreate" name="unitRentCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="unitRentEdit" name="unitRentEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="unitRentDelete" name="unitRentDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Maintenances</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="maintenances" name="maintenances"  /></td>
                            <td>Maintenances</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="maintenancesShow" name="maintenancesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="maintenancesCreate" name="maintenancesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="maintenancesEdit" name="maintenancesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="maintenancesDelete" name="maintenancesDelete" disabled  /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Transactions</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billings" name="billings"  /></td>
                            <td>Billings</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billingsShow" name="billingsShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billingsCreate" name="billingsCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billingsEdit" name="billingsEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billingsDelete" name="billingsDelete" disabled  /></td>
                          </tr> 
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoices" name="invoices"  /></td>
                            <td>Invoices</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesShow" name="invoicesShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesCreate" name="invoicesCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesEdit" name="invoicesEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="invoicesDelete" name="invoicesDelete" disabled  /></td>
                          </tr> 
                          <tr>
                            <td></td>
                            <td colSpan="5"><b>Accounting</b></td>
                          </tr>
                          <tr>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="journal" name="journal"  /></td>
                            <td>Journal</td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="journalShow" name="journalShow" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="journalCreate" name="journalCreate" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="journalEdit" name="journalEdit" disabled  /></td>
                            <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="journalDelete" name="journalDelete" disabled  /></td>
                          </tr> 
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddUserRoles;
