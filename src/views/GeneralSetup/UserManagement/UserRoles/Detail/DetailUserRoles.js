import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailUserRoles extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="User Roles" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Role Name</Label>
                      <FormText className="help-block">admin</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="12">
                  <Table responsive bordered>
                        <thead>
                          <tr>
                            <td><b>Menu Name</b></td>
                            <td width="50"><b>Show</b></td>
                            <td width="50"><b>Create</b></td>
                            <td width="50"><b>Edit</b></td>
                            <td width="50"><b>Delete</b></td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colSpan="5"><b>Settings</b></td>
                          </tr>
                          <tr>
                            <td>App Config</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Master Data</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>General Setup</b></td>
                          </tr>
                          <tr>
                            <td>Account</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Budget</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Complaints</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Organizations</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Invoices</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Meters</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Services</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Suppliers</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>User Management</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Doc Number Format</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Tax Settings</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Warehouse</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Leasable Area</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>Items</b></td>
                          </tr>
                          <tr>
                            <td>Equipments</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Materials</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>Profile</b></td>
                          </tr>
                          <tr>
                            <td>Companies</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Companies Properties</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Companies Units</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td>Owner & Tenant</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>Tenancy</b></td>
                          </tr>
                          <tr>
                            <td>Unit Rent</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>Maintenances</b></td>
                          </tr>
                          <tr>
                            <td>Maintenances</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr>
                          <tr>
                            <td colSpan="5"><b>Transactions</b></td>
                          </tr>
                          <tr>
                            <td>Billings</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr> 
                          <tr>
                            <td>Invoices</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr> 
                          <tr>
                            <td colSpan="5"><b>Accounting</b></td>
                          </tr>
                          <tr>
                            <td>Journal</td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                            <td className="text-center"><i class="fa fa-check"></i></td>
                          </tr> 
                        </tbody>
                      </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailUserRoles;
