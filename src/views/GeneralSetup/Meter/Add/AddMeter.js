import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddMeter extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Meter Group" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Meter</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="groupType">Meter Group</Label>
                        <InputGroup>  
                          <Input type="select" name="meterGroup" id="meterGroup">
                            <option value="0">Choose Meter Group</option>
                            <option value="1">MG 1300 -- Meter Group 1300</option>
                            <option value="2">MG 2000 -- Meter Group 2000</option>
                            <option value="2">MG 3600 -- Meter Group 3600</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select meter group</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterCode">Meter Code</Label>
                        <Input type="text" id="meterCode" name="meterCode" placeholder="Enter Meter Code.." />
                        <FormText className="help-block">Please enter meter code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterPower">Meter Power</Label>
                        <Input type="text" id="meterPower" name="meterPower" placeholder="Enter Meter Power.." />
                        <FormText className="help-block">Please enter meter Power</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterFixCharge">Meter Fix Charge</Label>
                        <Input type="number" id="meterFixCharge" name="meterFixCharge" placeholder="Enter Meter Fix Charge.." />
                        <FormText className="help-block">Please enter meter fix charge</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterCharge">Meter Charge</Label>
                        <Input type="number" id="meterCharge" name="meterCharge" placeholder="Enter Meter Charge.." />
                        <FormText className="help-block">Please enter meter charge</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="publicCharge">Public Charge</Label>
                        <Input type="number" id="publicCharge" name="publicCharge" placeholder="Enter Public Charge.." />
                        <FormText className="help-block">Please enter public charge</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="remark">Remark</Label>
                        <Input type="text" id="remark" name="remark" placeholder="Enter Remark.." />
                        <FormText className="help-block">Please enter remark</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMeter;
