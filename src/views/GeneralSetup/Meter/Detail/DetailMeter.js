import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailMeter extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Meter" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Group</Label>
                      <FormText className="help-block">Meter Group 1300</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Code</Label>
                      <FormText className="help-block">EL1001</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Power</Label>
                      <FormText className="help-block">1300</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Fix Charge</Label>
                      <FormText className="help-block">1000</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Charge</Label>
                      <FormText className="help-block">1500</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Public Charge</Label>
                      <FormText className="help-block">5000</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                <Col md="4">
                  <FormGroup>
                    <Label>Remark</Label>
                      <FormText className="help-block">OK</FormText>
                  </FormGroup>
                </Col>
                <Col md="4">
                  <FormGroup>
                    <Label>Status</Label>
                    <FormText className="help-block">Active</FormText>
                  </FormGroup>
                </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailMeter;
