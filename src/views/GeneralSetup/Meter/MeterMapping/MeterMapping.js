import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, companyName: 'HURRICANE BUILDING - EL001 ', meterCode: 'EL001', meterGroup:'Meter Group 1300', remark:''},
  { id: 2, companyName: 'HURRICANE BUILDING - M2000 ', meterCode: 'M2000', meterGroup:'Meter Group 2000', remark:''},
];
const columns = [
  {
    name: 'Company Name',
    selector: 'companyName',
    sortable: true,
  },
  {
    name: 'Meter Code',
    selector: 'meterCode',
    sortable: true,
  },
  {
    name: 'Meter Group',
    selector: 'meterGroup',
    sortable: true,
  },
  {
    name: 'Remark',
    selector: 'remark',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class MeterMapping extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Meter Mapping" />
        <DataTable
          title="Data Meter Group"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default MeterMapping;
