import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddMeterMapping extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Meter Mapping" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>New Meter Mapping</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="groupType">Meter Group</Label>
                        <Input type="select" name="meterGroup" id="meterGroup">
                          <option value="0">Choose Meter</option>
                          <option value="1">MG 1300 -- Meter Group 1300</option>
                          <option value="2">MG 2000 -- Meter Group 2000</option>
                          <option value="2">MG 3600 -- Meter Group 3600</option>
                        </Input>
                        <FormText className="help-block">Please choose meter group</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterCode">Meter Code</Label>
                        <Input type="select" id="meterCode" name="meterCode">
                          <option value="0">Choose Meter Code</option>
                        </Input>
                        <FormText className="help-block">Please choose meter code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="remark">Remark</Label>
                        <Input type="text" id="remark" name="remark" placeholder="Enter Remark.." />
                        <FormText className="help-block">Please enter remark</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                      <Col md="4">
                          <FormGroup>
                            <Label htmlFor="companyProperty">Company Property</Label>
                            <Input type="select" name="companyProperty" id="companyProperty">
                              <option value="0">Choose Property</option>
                              <option value="1">HRC -- HURRICANE BUILDING</option>
                              <option value="2">HRCA -- HURRICANE APARTMENT</option>
                            </Input>
                            <FormText className="help-block">Please choose property</FormText>
                          </FormGroup>
                      </Col>
                      <Col md="8">
                        <Label>Property Unit</Label>
                        <Table responsive bordered>
                          <tbody>
                            <tr>
                              <td className="text-center"> 
                                <Input className="form-check-input" type="checkbox" id="propertyUnit" name="propertyUnit" />
                              </td>
                              <td>Unit Type</td>
                              <td>Unit Code</td>
                            </tr>
                          </tbody>
                        </Table>
                      </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMeterMapping;
