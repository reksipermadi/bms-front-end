import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailMeterMapping extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Meter Mapping" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Group </Label>
                      <FormText className="help-block">Meter Group 1300</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Meter Code</Label>
                      <FormText className="help-block">EL001</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Remark</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Company Property</Label>
                      <FormText className="help-block">HURICANE BUILDING</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <Label>Property Unit</Label>
                    <Table responsive bordered>
                      <thead>
                        <tr>
                          <td>Unit Type</td>
                          <td>Unit Code</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>UNIT </td>
                          <td>A0101</td>
                        </tr>
                        <tr>
                          <td>UNIT </td>
                          <td>A0102</td>
                        </tr>
                        <tr>
                          <td>UNIT </td>
                          <td>A0103</td>
                        </tr>
                        <tr>
                          <td>UNIT </td>
                          <td>A0104</td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailMeterMapping;
