import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, meterCode: 'EL001', power: '1300', meterFixCharge: '1000', meterCharge:'1500', publicCharge:'5000', active:'Active'},
  { id: 2, meterCode: 'EL002', power: '1301', meterFixCharge: '2000', meterCharge:'2500', publicCharge:'4000', active:'Active'},
];
const columns = [
  {
    name: 'Meter Code',
    selector: 'meterCode',
    sortable: true,
  },
  {
    name: 'Power',
    selector: 'power',
    sortable: true,
  },
  {
    name: 'Meter Fix Charge',
    selector: 'meterFixCharge',
    sortable: true,
  },
  {
    name: 'Meter Charge',
    selector: 'meterCharge',
    sortable: true,
  },
  {
    name: 'Public Charge',
    selector: 'publicCharge',
    sortable: true,
  },
  {
    name: 'Active',
    selector: 'active',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Meter extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Meter" />
        <Card>
          <CardBody>
            <DataTable
              title="Data Meter"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default Meter;
