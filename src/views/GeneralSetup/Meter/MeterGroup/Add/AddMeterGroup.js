import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddMeterGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Meter Group" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Meter Group</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterGroupCode">Meter Group Code</Label>
                        <Input type="text" id="meterGroupCode" name="meterGroupCode" placeholder="Enter meter group code.." />
                        <FormText className="help-block">Please enter meter group code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="meterGroupName">Meter Group Name</Label>
                        <Input type="text" id="meterGroupName" name="meterGroupName" placeholder="Enter meter group name.." />
                        <FormText className="help-block">Please enter meter group name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddMeterGroup;
