import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddDepartment extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Department" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Department</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="division">Division</Label>
                        <InputGroup>  
                          <Input type="select" name="division" id="division">
                            <option value="0">Choose Division</option>
                            <option value="1">Division 1</option>
                            <option value="2">Division 2</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select division</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="departmentName">Department Name</Label>
                        <Input type="text" id="departmentName" name="departmentName" placeholder="Enter department name.." />
                        <FormText className="help-block">Please enter department name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="personInCharge">Person In Charge</Label>
                        <Input type="text" id="personInCharge" name="personInCharge" placeholder="Enter person in charge.." />
                        <FormText className="help-block">Please enter person in charge</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="position">Position</Label>
                        <Input type="text" id="position" name="position" placeholder="Enter position.." />
                        <FormText className="help-block">Please enter position</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="phone">Phone</Label>
                        <Input type="text" id="phone" name="phone" placeholder="Enter phone.." />
                        <FormText className="help-block">Please enter phone</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="email" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddDepartment;
