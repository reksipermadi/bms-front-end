import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, departmentName: 'Department 1', divisionName: 'Division 1', personInCharge: 'Anggi', position: 'admin', phone: '021 1234567', email: 'anggi@mail.com'},
  { id: 2, departmentName: 'Department 2', divisionName: 'Division 2', personInCharge: 'Asep', position: 'CEO', phone: '021 12345232', email: 'asep@ceo.com'},
];
const columns = [
  {
    name: 'Department Name',
    selector: 'departmentName',
    sortable: true,
  },
  {
    name: 'Division Name',
    selector: 'divisionName',
    sortable: true,
  },
  {
    name: 'Person In Charge',
    selector: 'personInCharge',
    sortable: true,
  },
  {
    name: 'Position',
    selector: 'position',
    sortable: true,
  },
  {
    name: 'Phone',
    selector: 'phone',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Department extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Department" />
        <DataTable
          title="Data Department"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default Department;
