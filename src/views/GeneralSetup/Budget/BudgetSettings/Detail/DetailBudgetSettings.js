import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailBudgetSettings extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Budget Settings" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Property</Label>
                      <FormText className="help-block">HURICANE BUILDING</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Budget Group</Label>
                      <FormText className="help-block">Budget Group 2</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Account</Label>
                      <FormText className="help-block">1.01.01.01.02 -- Kas Kecil</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Budget Code</Label>
                      <FormText className="help-block">BDGT01</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Budget Name</Label>
                      <FormText className="help-block">Budget Setting 1</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Month Period</Label>
                      <FormText className="help-block">January</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Year Period</Label>
                      <FormText className="help-block">2019</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Amount</Label>
                      <FormText className="help-block">500,000</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Remark</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Status</Label>
                      <FormText className="help-block">Active</FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailBudgetSettings;
