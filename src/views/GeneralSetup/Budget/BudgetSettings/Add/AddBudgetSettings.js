import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddBudgetSettings extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Bank Account" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Bank Account</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="property">Property</Label>
                        <InputGroup>  
                          <Input type="select" name="property" id="property">
                            <option value="0">Choose Property</option>
                            <option value="1">HRC - Huricane Building</option>
                            <option value="2">HRC - Huricane Apartment</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="budgetGroup">Budget Group</Label>
                        <InputGroup>  
                          <Input type="select" name="budgetGroup" id="budgetGroup">
                            <option value="0">Choose Budget Group</option>
                            <option value="1">BG0001 - Budget Group 1</option>
                            <option value="2">BG0002 - Budget Group 2</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                        <Input type="select" name="account" id="account">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="budgetCode">Budget Code</Label>
                        <Input type="text" id="budgetCode" name="budgetCode" placeholder="Enter budget code.." />
                        <FormText className="help-block">Please enter budget code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="budgetName">Budget Name</Label>
                        <Input type="text" id="budgetName" name="budgetName" placeholder="Enter budget name.." />
                        <FormText className="help-block">Please enter budget name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="monthPeriod">Month Period</Label>
                        <Input type="select" name="monthPeriod" id="monthPeriod">
                          <option value="0">Choose Month</option>
                          <option value="1">January</option>
                          <option value="2">February</option>
                          <option value="3">March</option>
                        </Input>
                        <FormText className="help-block">Please select month</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="yearPeriod">Year Period</Label>
                        <Input type="select" name="yearPeriod" id="yearPeriod">
                          <option value="0">Choose Year</option>
                          <option value="1">2018</option>
                          <option value="2">2019</option>
                          <option value="3">2020</option>
                        </Input>
                        <FormText className="help-block">Please select year</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="amount">Amount</Label>
                        <Input type="text" id="amount" name="amount" placeholder="Enter amount.." />
                        <FormText className="help-block">Please enter amount</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="remark">Remark</Label>
                        <Input type="text" id="remark" name="remark" placeholder="Enter Remark.." />
                        <FormText className="help-block">Please enter remark</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <Label>Status</Label>
                      <FormGroup check className="checkbox">
                        <Input className="form-check-input" type="checkbox" id="status" name="status" value="active" />
                        <Label check className="form-check-label" htmlFor="status">Active</Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddBudgetSettings;
