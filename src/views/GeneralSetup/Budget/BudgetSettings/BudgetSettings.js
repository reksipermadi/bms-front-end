import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, budgetCode: 'BDGT01', budgetName: 'Budget Settings 1', budgetGroup: 'Budget Group 2', monthPeriod: 'January', propertyName: 'HURICANE BUILDING', status: 'active' },
  { id: 2, budgetCode: 'BDGT02', budgetName: 'Budget Settings 2', budgetGroup: 'Budget Group 1', monthPeriod: 'February', propertyName: 'HURICANE BUILDING', status: 'active' },
];
const columns = [
  {
    name: 'Budget Code',
    selector: 'budgetCode',
    sortable: true,
  },
  {
    name: 'Budget Name',
    selector: 'budgetName',
    sortable: true,
  },
  {
    name: 'Budget Group',
    selector: 'budgetGroup',
    sortable: true,
  },
  {
    name: 'Month Period',
    selector: 'monthPeriod',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class BudgetSettings extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Budget Settings" />
        <DataTable
          title="Data Budget Settings"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default BudgetSettings;
