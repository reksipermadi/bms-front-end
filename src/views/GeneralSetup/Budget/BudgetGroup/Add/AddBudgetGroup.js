import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddBudgetGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Budget Group" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Budget Group</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="budgetGroupParent">Budget Group Parent</Label>
                        <Input type="select" name="budgetGroupParent" id="budgetGroupParent">
                          <option value="0">Choose Parent</option>
                          <option value="1">Budget Group 1</option>
                          <option value="2">Budget Group 2</option>
                          <option value="3">Budget Group 3</option>
                        </Input>
                        <FormText className="help-block">Please select budget group parent</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="budgetGroupCode">Budget Group Code</Label>
                        <Input type="text" id="budgetGroupCode" name="babudgetGroupCodenkCode" placeholder="Enter budget group code.." />
                        <FormText className="help-block">Please enter budget group code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="budgetGroupName">Budget Group Name</Label>
                        <Input type="text" id="budgetGroupName" name="budgetGroupName" placeholder="Enter budget group name.." />
                        <FormText className="help-block">Please enter budget group name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="account">Account</Label>
                        <Input type="select" name="account" id="account">
                          <option value="0">Choose Account</option>
                          <option value="1">1 - AKTIVA</option>
                          <option value="2">1.01 - Aset Lancar</option>
                        </Input>
                        <FormText className="help-block">Please select account</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddBudgetGroup;
