import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, budgetGroupCode: 'BG0001', budgetGroupName: 'Budget Group 1', budgetGroupParent: '', account: '-- 1.01.01.02.02.01 -- Kas Kecil'},
  { id: 2, budgetGroupCode: 'BG0002', budgetGroupName: 'Budget Group 2', budgetGroupParent: '', account: '-- 1.01.01.02.02.01 -- Kas Kecil'},
  { id: 3, budgetGroupCode: 'BG0003', budgetGroupName: 'Budget Group 3', budgetGroupParent: '', account: '-- 1.01.01.02.02.01 -- Kas Kecil'},
];
const columns = [
  {
    name: 'Budget Group Code',
    selector: 'budgetGroupCode',
    sortable: true,
  },
  {
    name: 'Budget Group Name',
    selector: 'budgetGroupName',
    sortable: true,
  },
  {
    name: 'Budget Group Parent',
    selector: 'budgetGroupParent',
    sortable: true,
  },
  {
    name: 'Account',
    selector: 'account',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class BudgetGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Budget Group" />
        <Card>
          <CardBody>
            <DataTable
              title="Budget Group"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
        <br/>
      </div>
    );
  }
}

export default BudgetGroup;
