import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddComplaintIssue extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Complaint Issue" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Complaint Issue</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="complaintCategory">Complaint Category</Label>
                        <Input type="select" name="complaintCategory" id="complaintCategory">
                          <option value="0">Choose Category</option>
                          <option value="1">CPLT10001 - Maintenance Air Conditioner</option>
                        </Input>
                        <FormText className="help-block">Please select complaint category</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="code">Code</Label>
                        <Input type="text" id="code" name="code" placeholder="Enter code.." />
                        <FormText className="help-block">Please enter code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="6">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddComplaintIssue;
