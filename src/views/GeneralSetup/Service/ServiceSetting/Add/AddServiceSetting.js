import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddServiceSetting extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Service Setting" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Service Setting</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="serviceGroup">Service Group</Label>
                        <InputGroup>  
                          <Input type="select" name="serviceGroup" id="serviceGroup">
                            <option value="0">Choose Service Group</option>
                            <option value="1">SG0001</option>
                            <option value="2">SG0002</option>
                          </Input>
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="primary"><i class="fa fa-plus"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please select service Group</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="serviceName">Service Name</Label>
                        <Input type="text" id="serviceName" name="serviceName" placeholder="Enter service name.." />
                        <FormText className="help-block">Please enter service name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="fee">Fee</Label>
                        <Input type="text" id="fee" name="fee" placeholder="Enter fee.." />
                        <FormText className="help-block">Please enter fee</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddServiceSetting;
