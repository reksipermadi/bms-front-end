import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailServiceGroup extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Service Group" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Service Group Name</Label>
                      <FormText className="help-block">SG0001</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Description</Label>
                      <FormText className="help-block">Service Group 1</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>Account</Label>
                      <FormText className="help-block">1.01.01.01.02.01 -- Kas Kecil</FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailServiceGroup;
