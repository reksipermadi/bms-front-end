import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddCountry extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Country" />
        <Row>
          <Col md="6">
            <Card>
              <CardHeader>
                <strong>Add Country</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="countryID">Country ID</Label>
                    <Input type="text" id="countryID" name="countryID" placeholder="Enter Country ID.." />
                    <FormText className="help-block">Please enter country id</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="countryName">Country Name</Label>
                    <Input type="text" id="countryName" name="countryName" placeholder="Enter Country Name.." />
                    <FormText className="help-block">Please enter country name</FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddCountry;
