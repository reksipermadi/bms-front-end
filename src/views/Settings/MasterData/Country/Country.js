import React, { Component } from 'react';
import { Input, Button } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, countryCode: 'ID', countryName: 'Indonesia' },
  { id: 2, countryCode: 'SG', countryName: 'Singapura' },
  { id: 3, countryCode: 'MY', countryName: 'Malaysia' },
];
const columns = [
  {
    name: 'Country Code',
    selector: 'countryCode',
    sortable: true,
  },
  {
    name: 'Country Name',
    selector: 'countryName',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Country extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Country" />
        <DataTable
          title="Data Country"
          columns={columns}
          data={data}
          actions={actions}
          subHeader
          subHeaderComponent={
            (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <Input type="text" name="Search" placeholder="Search" />
              </div>
            )
          }
          pagination
          highlightOnHover
        />
        <br/>
      </div>
    );
  }
}

export default Country;
