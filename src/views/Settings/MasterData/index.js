import Province from './Province';
import AddProvince from './Province/Add';
import DetailProvince from './Province/Detail';
import City from './City';
import AddCity from './City/Add';
import Country from './Country';

export {
  Province, AddProvince, DetailProvince, City, AddCity, Country,
};