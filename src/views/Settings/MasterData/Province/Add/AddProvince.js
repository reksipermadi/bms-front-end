import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddProvince extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Province" />
        <Row>
          <Col md="6">
            <Card>
              <CardHeader>
                <strong>Add Province</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="province">Province Name</Label>
                    <Input type="text" id="province" name="province" placeholder="Enter Province.." />
                    <FormText className="help-block">Please enter province name</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="country">Country</Label>
                    <Input type="select" name="country" id="country">
                      <option value="1">ID-Indonesia</option>
                      <option value="2">SG-Singapure</option>
                      <option value="3">MY-Malaysia</option>
                    </Input>
                    <FormText className="help-block">Please select country</FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddProvince;
