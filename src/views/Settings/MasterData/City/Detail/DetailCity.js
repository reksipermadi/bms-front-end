import React, { Component } from 'react';
import { Card, CardBody, Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class DetailProvince extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="City" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>City Name</Label>
                      <FormText className="help-block">Jakarta Barat</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Province Name</Label>
                      <FormText className="help-block">DKI Jakarta</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Country</Label>
                      <FormText className="help-block">ID - Indonesia</FormText>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailProvince;
