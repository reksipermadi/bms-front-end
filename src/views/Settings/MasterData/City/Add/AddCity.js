import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../../Shared/HeadTitle/HeadTitle';

class AddCity extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="City" />
        <Row>
          <Col md="6">
            <Card>
              <CardHeader>
                <strong>Add City</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="city">City Name</Label>
                    <Input type="text" id="city" name="city" placeholder="Enter City.." />
                    <FormText className="help-block">Please enter city name</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="province">Province</Label>
                    <Input type="select" name="province" id="province">
                      <option value="1">DKI Jakarta</option>
                      <option value="2">Banten</option>
                      <option value="3">Jawa Baarat</option>
                    </Input>
                    <FormText className="help-block">Please select province</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="country">Country</Label>
                    <Input type="select" name="country" id="country">
                      <option value="1">ID-Indonesia</option>
                      <option value="2">SG-Singapure</option>
                      <option value="3">MY-Malaysia</option>
                    </Input>
                    <FormText className="help-block">Please select country</FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddCity;
