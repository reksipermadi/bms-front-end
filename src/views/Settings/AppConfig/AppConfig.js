import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, Input, Label, FormGroup, Button } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';

class AppConfig extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="App Config" />
        <Row>
        <Col xs="12">
            <Card>
              <CardHeader>
                <strong>Profile</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="6">
                    <FormGroup>
                      <Label htmlFor="companyName">Company Name</Label>
                      <Input type="text" id="companyName" name="companyName" placeholder="Enter your company name" value="BMS Company" />
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="address">Address</Label>
                      <Input type="textarea" name="address" id="address" rows="9" placeholder="Enter your address" value="Cileungsi" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6">
                    <FormGroup>
                      <Label htmlFor="email">Email</Label>
                      <Input type="email" id="email" name="email" placeholder="yourmail@mail.com" value="admin@bms.com" />
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="phone1">Phone 1</Label>
                      <Input type="text" name="phone1" id="phone1" placeholder="081xxxxx" value="(021) 12345678" />
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="phone2">Phone 2</Label>
                      <Input type="text" name="phone2" id="phone2" placeholder="081xxxxx" value="-" />
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="fax">Fax</Label>
                      <Input type="text" name="fax" id="fax" placeholder="081xxxxx" value="(021) 87654321" />
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                <strong>Tax</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="4">
                    <FormGroup>
                      <Label htmlFor="ppn">Ppn (%)</Label>
                      <Input type="number" name="ppn" id="ppn" value="10" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="8">
                    <FormGroup>
                      <Label htmlFor="ppnAccount">Ppn Account No</Label>
                      <Input type="select" name="ppnAccount" id="ppnAccount">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="4">
                    <FormGroup>
                      <Label htmlFor="pph21">Pph 21 (%)</Label>
                      <Input type="number" name="pph21" id="pph21" value="0" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="8">
                    <FormGroup>
                      <Label htmlFor="ppnAccount21">Ppn 21 Account No</Label>
                      <Input type="select" name="ppnAccount21" id="ppnAccount21">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="4">
                    <FormGroup>
                      <Label htmlFor="pph23">Ppn 23 (%)</Label>
                      <Input type="number" name="pph23" id="pph23" value="0" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="8">
                    <FormGroup>
                      <Label htmlFor="ppnAccount23">Ppn 23 Account No</Label>
                      <Input type="select" name="ppnAccount23" id="ppnAccount23">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="4">
                    <FormGroup>
                      <Label htmlFor="pphFinal">Ppn Final (%)</Label>
                      <Input type="number" name="pphFinal" id="pphFinal" value="0" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="8">
                    <FormGroup>
                      <Label htmlFor="ppnAccountFinal">Ppn Final Account No</Label>
                      <Input type="select" name="ppnAccountFinal" id="ppnAccountFinal">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>

            <Card>
              <CardHeader>
                <strong>Stamp</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="2">
                    <FormGroup>
                      <Label htmlFor="stampAmount">Stamp Amount</Label>
                      <Input type="text" name="stampAmount" id="stampAmount" value="6,000" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6">
                    <FormGroup>
                      <Label htmlFor="stampDebet">Stamp Debet Account</Label>
                      <Input type="select" name="stempDebet" id="stempDebet">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6">
                    <FormGroup>
                      <Label htmlFor="stampCredit">Stamp Credit Account</Label>
                      <Input type="select" name="stempCredit" id="stempCredit">
                        <option value="1">1.01 -- Aset Lancar</option>
                        <option value="2">1.01.01 -- Kas dan Setara Kas</option>
                        <option value="3">1.01.01.01 -- Kas</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
            <div class="text-right mb-3">
              <Button type="cancel" size="md" color="danger" className="mr-2"><i className="fa fa-ban"></i> Cancel</Button>
              <Button type="submit" size="md" color="primary"><i className="fa fa-dot-circle-o"></i> Save</Button>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AppConfig;
