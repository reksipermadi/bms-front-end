import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, unitCode: 'A0005', propertyName: 'HURICANE BUILDING', ownerName: 'REZA', tenantName: 'Eko Patrio', status: 'Rented'},
  { id: 2, unitCode: 'A0006', propertyName: 'HURICANE BUILDING', ownerName: 'REZA', tenantName: 'Sheeza Ramadhina Khadafi', status: 'Rented'},
  { id: 3, unitCode: 'A0007', propertyName: 'HURICANE BUILDING', ownerName: 'REZA', tenantName: 'Eko Patrio', status: 'For Rent'},
];
const columns = [
  {
    name: 'Unit Code',
    selector: 'unitCode',
    sortable: true,
  },
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Owner Name',
    selector: 'ownerName',
    sortable: true,
  },
  {
    name: 'Tenant Name',
    selector: 'tenantName',
    sortable: true,
  },
  {
    name: 'Status',
    selector: 'status',
    sortable: true,
  },
  {
    name: 'Movein / Moveout',
    cell: () => <Button color="danger">Move out</Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

class UnitRent extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Unit Rent" />
            <DataTable
              title="Data Unit Rent"
              columns={columns}
              data={data}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default UnitRent;
