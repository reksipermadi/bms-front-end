import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailBillings extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Billings" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                </div>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Tenant & Unit</h3>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Property</Label>
                      <FormText className="help-block">HURICANE BUILDING</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Unit</Label>
                      <FormText className="help-block">A0005</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Owner Name</Label>
                      <FormText className="help-block">Reza</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Owner Company</Label>
                      <FormText className="help-block">PT.BMS</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Tenant Name</Label>
                      <FormText className="help-block">Eko Patrio</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Tenant Company</Label>
                      <FormText className="help-block">PT Maju Mundur</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Begin Date Lease</Label>
                      <FormText className="help-block">01-08-2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>End Date Lease</Label>
                      <FormText className="help-block">31-10-2019</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Billings</h3>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered hover>
                      <thead>
                        <th></th>
                        <th>Type</th>
                        <th>Start Invoice</th>
                        <th>Invoice Category</th>
                        <th>Invoice Schedule</th>
                        <th>Due Days</th>
                        <th>Due Date</th>
                        <th>Amount</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billings[0]" name="billings[0]" /></td>
                          <td>Invoice</td>
                          <td>05/08/2019</td>
                          <td>AR/ Service Charge</td>
                          <td>Monthly</td>
                          <td>7 Days</td>
                          <td>12/08/2019</td>
                          <td>500,000.00</td>
                        </tr>
                        <tr>
                        <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billings[1]" name="billings[1]" /></td>
                          <td>Invoice</td>
                          <td>05/08/2019</td>
                          <td>AR/ Rent Building</td>
                          <td>Monthly</td>
                          <td>7 Days</td>
                          <td>12/08/2019</td>
                          <td>2,000,000.00</td>
                        </tr>
                        <tr>
                          <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billings[2]" name="billings[2]" /></td>
                          <td>Invoice</td>
                          <td>04/09/2019</td>
                          <td>AR/ Service Charge</td>
                          <td>Monthly</td>
                          <td>7 Days</td>
                          <td>11/09/2019</td>
                          <td>500,000.00</td>
                        </tr>
                        <tr>
                          <td className="text-center"><Input className="form-check-input single-check" type="checkbox" id="billings[3]" name="billings[3]" /></td>
                          <td>Invoice</td>
                          <td>04/09/2019</td>
                          <td>AR/ Rent Building</td>
                          <td>Monthly</td>
                          <td>7 Days</td>
                          <td>11/09/2019</td>
                          <td>2,000,000.00</td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                </Row>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="dueDate">Due Date</Label>
                      <InputGroup>  
                        <Input type="date" id="dueDate" name="dueDate" placeholder="Enter due date.." disabled />
                        <InputGroupAddon addonType="append">
                          <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                        </InputGroupAddon>
                      </InputGroup>
                      <FormText className="help-block">Please enter due date</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>Include</Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Input className="form-check-input" type="checkbox" id="ppn" name="ppn" value="ppn" disabled />
                      <Label className="form-check-label" check htmlFor="ppn">Ppn</Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Input className="form-check-input" type="checkbox" id="stamp" name="stamp" value="stamp" disabled />
                      <Label className="form-check-label" check htmlFor="stamp">Stamp</Label>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="note">Note</Label>
                      <Input type="textarea" id="note" name="note" placeholder="Enter note.." disabled />
                      <FormText className="help-block">Please enter note</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12">
                    <Button color="warning" disabled><i class="fa fa-gears"></i> Generate Invoice</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailBillings;
