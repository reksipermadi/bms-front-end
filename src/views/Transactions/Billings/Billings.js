import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, tenant: 'Eko Patrio', property: 'HURICANE BUILDING', unit: 'A0005', totalBilling: '10'},
  { id: 2, tenant: 'Eko Patrio', property: 'HURICANE BUILDING', unit: 'A0102', totalBilling: '10'},
  { id: 3, tenant: 'Sheeza Ramadhina Khadafi', property: 'HURICANE BUILDING', unit: 'A0101', totalBilling: '10'},
];
const columns = [
  {
    name: 'Tenant',
    selector: 'tenant',
    sortable: true,
  },
  {
    name: 'Property',
    selector: 'property',
    sortable: true,
  },
  {
    name: 'Unit',
    selector: 'unit',
    sortable: true,
  },
  {
    name: 'Total Billing',
    selector: 'totalBilling',
    sortable: true,
  },
  {
    name: 'Detail',
    cell: () => <Button color="primary"><i class="fa fa-search fa-lg"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

class Billings extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Billings" />
            <DataTable
              title="Billings"
              columns={columns}
              data={data}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Billings;
