import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailInvoices extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Invoices" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="danger"><i class="fa fa-dollar"></i> Payment</Button>
                </div>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Invoice</h3>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Tenant Name</Label>
                      <FormText className="help-block">Eko Patrio</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice No.</Label>
                      <FormText className="help-block">INV/022/08/19</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice Due Date</Label>
                      <FormText className="help-block">08/21/2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Note</Label>
                      <FormText className="help-block">invoice Agustus 2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Unit / Property</Label>
                      <FormText className="help-block">A0005 / HURICANE BUILDING</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice Date</Label>
                      <FormText className="help-block">08/15/2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Include</Label>
                      <FormText className="help-block">
                        <i class="fa fa-check text-success"></i> Ppn 
                        <i class="fa fa-check text-success"></i> Stamp
                      </FormText>
                    </FormGroup>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered>
                      <thead>
                        <th>Invoice Name</th>
                        <th>Start Invoice</th>
                        <th>Amount</th>
                        <th>Balance</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>AR/ Rent Building</td>
                          <td>08/05/2019</td>
                          <td className="text-right">2,000,000</td>
                          <td className="text-right">1,000,000</td>
                        </tr>
                        <tr>
                          <td>AR/ Service Charge</td>
                          <td>08/05/2019</td>
                          <td className="text-right">500,000</td>
                          <td className="text-right">400,000</td>
                        </tr>
                        <tr>
                          <td>Deposit Infront</td>
                          <td>08/05/2019</td>
                          <td className="text-right">1,000,000</td>
                          <td className="text-right">1,000,000</td>
                        </tr>
                        <tr>
                          <td>Security Deposit</td>
                          <td>08/05/2019</td>
                          <td className="text-right">200,000</td>
                          <td className="text-right">200,000</td>
                        </tr>
                        <tr>
                          <th colSpan="2" className="text-right">Ppn</th>
                          <th className="text-right">370,000</th>
                          <td></td>
                        </tr>
                        <tr>
                          <th colSpan="2" className="text-right">Stamp</th>
                          <th className="text-right">6,000</th>
                          <td></td>
                        </tr>
                        <tr>
                          <th colSpan="2" className="text-right">Total</th>
                          <th className="text-right">4,076,000</th>
                          <td></td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Invoice Payment History</h3>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered hover>
                      <thead>
                        <th>Payment Date</th>
                        <th>Payment Method</th>
                        <th>Payment Bank</th>
                        <th>Total Payment</th>
                        <th>Note</th>
                        <th>Print</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>08/15/2019</td>
                          <td>Transfer</td>
                          <td>BKP-CAWANG (09023908234598)</td>
                          <td className="text-right">1,100,000</td>
                          <td>bayar sebagian</td>
                          <td>
                            <Button type="button" color="primary" className="mr-2"><i class="fa fa-search"></i></Button>
                            <Button type="button" color="success"><i class="fa fa-print"></i></Button>
                          </td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailInvoices;
