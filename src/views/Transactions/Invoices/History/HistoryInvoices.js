import React, { Component } from 'react';
import { Card,  CardBody, Label, FormGroup, Button, FormText, Row, Col, Table, Input } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class HistoryInvoices extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="History" title="Invoices" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-print"></i> Print Invoice</Button>
                </div>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Invoice</h3>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Tenant Name</Label>
                      <FormText className="help-block">Eko Patrio</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice No.</Label>
                      <FormText className="help-block">INV/022/08/19</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice Due Date</Label>
                      <FormText className="help-block">08/21/2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Note</Label>
                      <FormText className="help-block">invoice Agustus 2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Unit / Property</Label>
                      <FormText className="help-block">A0005 / HURICANE BUILDING</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Invoice Date</Label>
                      <FormText className="help-block">08/15/2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Include</Label>
                      <FormText className="help-block">
                        <i class="fa fa-check text-success"></i> Ppn 
                        <i class="fa fa-check text-success"></i> Stamp
                      </FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="12 mb-2">
                      <h3 class="text-info">Payment Details</h3>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Payment Method</Label>
                      <FormText className="help-block">Transfer</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Bank</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Payment Date</Label>
                      <FormText className="help-block">08/15/2019</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="12">
                    <Table responsive bordered>
                      <thead>
                        <th>Inovice Name</th>
                        <th>Start Invoice</th>
                        <th>Payment</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>AR/ Rent Building</td>
                          <td>08/05/2019</td>
                          <td className="text-right">0</td>
                        </tr>
                        <tr>
                          <td>AR/ Service Charge</td>
                          <td>08/05/2019</td>
                          <td className="text-right">0</td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="note">Note</Label>
                      <Input type="textarea" id="note" name="note" value="bayar sebagian" disabled />
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default HistoryInvoices;
