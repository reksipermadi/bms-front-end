import React, { Component } from 'react';
import { Input, Button, Card, CardHeader, CardBody, Row, Col, FormGroup, Label } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, invoiceNo: 'INV/022/08/19', invoiceDate: '08/15/2019', tenant: 'Eko Patrio', property: 'HURICANE BUILDING', unit: 'A0005', totalAmount: '4,076,000', totalPaid: '1,100,000', balance: '2,976,000'},
  { id: 2, invoiceNo: 'INV/023/08/19', invoiceDate: '08/15/2019', tenant: 'Eko Patrio', property: 'HURICANE BUILDING', unit: 'A0102', totalAmount: '4,516,000', totalPaid: '0', balance: '4,516,000'},
  { id: 3, invoiceNo: 'INV/024/08/19', invoiceDate: '08/15/2019', tenant: 'Sheeza Ramadhina Khadafi', property: 'HURICANE BUILDING', unit: 'A0101', totalAmount: '2,976,000', totalPaid: '0', balance: '2,976,000'},
];
const columns = [
  {
    name: 'Invoice No',
    selector: 'invoiceNo',
    sortable: true,
  },
  {
    name: 'Invoice Date',
    selector: 'invoiceDate',
    sortable: true,
  },
  {
    name: 'Tenant',
    selector: 'tenant',
    sortable: true,
  },
  {
    name: 'Property',
    selector: 'property',
    sortable: true,
  },
  {
    name: 'Unit',
    selector: 'unit',
    sortable: true,
  },
  {
    name: 'Total Amount',
    selector: 'totalAmount',
    sortable: true,
  },
  {
    name: 'Total Paid',
    selector: 'totalPaid',
    sortable: true,
  },
  {
    name: 'Balance',
    selector: 'Balance',
    sortable: true,
  },
  {
    name: 'Detail',
    cell: () => <Button color="primary"><i class="fa fa-search fa-lg"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

class Invoices extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle title="Invoices" />
        <Row>
          <Col xs="12" sm="6" md="4">
            <Card className="bms-card-invoice">
              <CardHeader>
                Open Invoice
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="4" md="4">
                    <i className="icon-docs icons d-block"></i>
                  </Col>
                  <Col xs="12" sm="8" md="8">
                    <div className="bms-box-detail">
                      <p><b>Total Open Invoice</b></p>
                      <p class="font-2xl">4 Invoice</p>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" md="4">
            <Card className="bms-card-invoice">
              <CardHeader>
                Unpaid Invoice
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="4" md="4">
                    <p class="bms-icons-invoice">Rp</p>
                  </Col>
                  <Col xs="12" sm="8" md="8">
                    <div className="bms-box-detail">
                      <p><b>Total Unpaid Invoice</b></p>
                      <p class="font-2xl">Rp 11,024,000</p>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" md="4">
            <Card className="bms-card-invoice">
              <CardHeader>
                Paid Invoice
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" sm="4" md="4">
                    <p class="bms-icons-invoice">Rp</p>
                  </Col>
                  <Col xs="12" sm="8" md="8">
                    <div className="bms-box-detail">
                      <p><b>Total Paid Invoice</b></p>
                      <p class="font-2xl">Rp 1,100,000</p>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="invoiceStatus">Invoice Status</Label>
                      <Input type="select" name="invoiceStatus" id="invoiceStatus">
                        <option value="0">Choose All</option>
                        <option value="1">Open</option>
                        <option value="2">Paid</option>
                        <option value="3">Unpaid</option>
                        <option value="4">Refund</option>
                        <option value="5">Canceled</option>
                        <option value="6">Deleted</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="month">Month</Label>
                      <Input type="select" name="month" id="month">
                        <option value="0">Choose All</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label htmlFor="year">Year</Label>
                      <Input type="text" id="year" name="year" placeholder="Enter year.." value="2020" />
                    </FormGroup>
                  </Col>
                  <hr/>
                  <Col md="12">
                    <Button type="submit" color="primary"><i className="fa fa-filter"></i> Filter</Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <DataTable
                  title="Data Invoices"
                  columns={columns}
                  data={data}
                  subHeader
                  subHeaderComponent={
                    (
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Input type="text" name="Search" placeholder="Search" />
                      </div>
                    )
                  }
                  pagination
                  highlightOnHover
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Invoices;
