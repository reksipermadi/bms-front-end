import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, Table } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddCompanyUnits extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Company Units" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Company Units</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="property">Property</Label>
                        <Input type="select" name="property" id="property">
                          <option value="0">Choose Property</option>
                          <option value="1">HRC -- HURICANE BUILDING</option>
                          <option value="2">HRCA -- HURICANE APARTMENT</option>
                        </Input>
                        <FormText className="help-block">Please select property</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="owner">Owner</Label>
                        <Input type="select" name="owner" id="owner">
                          <option value="0">Choose Owner</option>
                          <option value="1">REZA -- PT. BMS</option>
                        </Input>
                        <FormText className="help-block">Please select owner</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="isLeasableArea">Is Leasable Area</Label>
                        <Input type="select" name="isLeasableArea" id="isLeasableArea">
                          <option value="0">Choose Leasable Area</option>
                          <option value="1">LA-001</option>
                        </Input>
                        <FormText className="help-block">Please select leasable area</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                    <Table responsive bordered>
                        <thead>
                          <tr>
                            <td>Unit No</td>
                            <td>Unit Size</td>
                            <td>Floor</td>
                            <td>Rent Amount</td>
                            <td>Deposit Amount</td>
                            <td width="50">Delete</td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <FormGroup>
                                <Input type="text" id="unitNo" name="phoneunitNoNumber" placeholder="Enter unit no.." />
                              </FormGroup>
                            </td>
                            <td>
                              <FormGroup>
                                <Input type="text" id="unitSize" name="unitSize" placeholder="Enter unit size.." />
                              </FormGroup>
                            </td>
                            <td>
                              <FormGroup>
                                <Input type="text" id="floor" name="floor" placeholder="Enter floor.." />
                              </FormGroup>
                            </td>
                            <td>
                              <FormGroup>
                                <Input type="number" id="rentAmount" name="rentAmount" placeholder="Enter rent amount.." />
                              </FormGroup>
                            </td>
                            <td>
                              <FormGroup>
                                <Input type="number" id="depositAmount" name="depositAmount" placeholder="Enter deposit amount.." />
                              </FormGroup>
                            </td>
                            <td></td>
                          </tr>
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="photo">Photo</Label>
                        <Input type="file" id="photo" name="photo" placeholder="Enter photo.." />
                        <FormText className="help-block">Please enter photo</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddCompanyUnits;
