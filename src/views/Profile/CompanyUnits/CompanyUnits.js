import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, unitNo: 'A0005', type: 'UNIT', totalLeasableArea: '', size: '21', floor: '1', rentAmount: '200,000', depositAmount: '0', property: 'HURICANE BUILDING', owner: 'PT.BMS' },
  { id: 2, unitNo: 'A0101', type: 'UNIT', totalLeasableArea: '', size: '33', floor: '1', rentAmount: '250,000', depositAmount: '0', property: 'HURICANE BUILDING', owner: 'PT.BMS' },
  { id: 3, unitNo: 'A0102', type: 'UNIT', totalLeasableArea: '', size: '21', floor: '1', rentAmount: '250,000', depositAmount: '0', property: 'HURICANE BUILDING', owner: 'PT.BMS' },
];
const columns = [
  {
    name: 'Unit No',
    selector: 'unitNo',
    sortable: true,
  },
  {
    name: 'Total Leasable Area',
    selector: 'totalLeasableArea',
    sortable: true,
  },
  {
    name: 'Size',
    selector: 'size',
    sortable: true,
  },
  {
    name: 'Floor',
    selector: 'floor',
    sortable: true,
  },
  {
    name: 'Rent Amount',
    selector: 'rentAmount',
    sortable: true,
  },
  {
    name: 'Deposit Amount',
    selector: 'depositAmount',
    sortable: true,
  },
  {
    name: 'Property',
    selector: 'property',
    sortable: true,
  },
  {
    name: 'Owner',
    selector: 'owner',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class CompanyUnits extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Company Units" />
            <DataTable
              title="Data Company Units"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default CompanyUnits;
