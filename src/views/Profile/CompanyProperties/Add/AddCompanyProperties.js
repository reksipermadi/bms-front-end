import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddCompanyProperties extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Company Properties" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Company Properties</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="company">Company</Label>
                        <Input type="select" name="company" id="company">
                          <option value="0">Choose Company</option>
                          <option value="1">PT. HURICANE INTERNATIONAL</option>
                        </Input>
                        <FormText className="help-block">Please select company</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="propertyCode">Property Code</Label>
                        <Input type="text" id="propertyCode" name="propertyCode" placeholder="Enter property code.." />
                        <FormText className="help-block">Please enter property code</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="propertyName">Property Name</Label>
                        <Input type="text" id="propertyName" name="propertyName" placeholder="Enter property name.." />
                        <FormText className="help-block">Please enter property name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="phoneNumber">Phone Number</Label>
                        <Input type="text" id="phoneNumber" name="phoneNumber" placeholder="Enter phone number.." />
                        <FormText className="help-block">Please enter phone number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="faxNumber">Fax Number</Label>
                        <Input type="text" id="faxNumber" name="faxNumber" placeholder="Enter fax number.." />
                        <FormText className="help-block">Please enter fax number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="website">Website</Label>
                        <Input type="text" id="website" name="website" placeholder="Enter website.." />
                        <FormText className="help-block">Please enter website</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="mobileNumber">Mobile Number</Label>
                        <Input type="text" id="mobileNumber" name="mobileNumber" placeholder="Enter mobile number.." />
                        <FormText className="help-block">Please enter mobile number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label htmlFor="address">Address</Label>
                        <Input type="textarea" id="address" name="address" placeholder="Enter address.." />
                        <FormText className="help-block">Please enter address</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="country">Country</Label>
                        <Input type="select" name="country" id="country">
                          <option value="0">Choose Country</option>
                          <option value="1">ID - Indonesia</option>
                        </Input>
                        <FormText className="help-block">Please select country</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="province">Province</Label>
                        <Input type="select" name="province" id="province">
                          <option value="0">Choose Province</option>
                        </Input>
                        <FormText className="help-block">Please select province</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="city">City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Choose City</option>
                        </Input>
                        <FormText className="help-block">Please select city</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="postalCode">Postal Code</Label>
                        <Input type="text" id="postalCode" name="postalCode" placeholder="Enter postalcode.." />
                        <FormText className="help-block">Please enter postal code</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="8">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="companyLogo">Company Logo</Label>
                        <Input type="file" id="companyLogo" name="companyLogo" placeholder="Enter company logo.." />
                        <FormText className="help-block">Please enter company logo</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="documents">Documents</Label>
                        <Input type="file" id="documents" name="documents" placeholder="Enter documents.." />
                        <FormText className="help-block">Please enter documents</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddCompanyProperties;
