import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, propertyName: 'HURICANE APARTMENT', company: 'PT. HURICANE INTERNATIONAL', phoneNo: '021-8934094', mobileNo: '083472349234', faxNo: '021-8934095', email: 'cs@hrca.com', address: 'Jl. Bambu Apus No.089', city: 'Jakarta Barat' },
  { id: 2, propertyName: 'HURICANE APARTMENT', company: 'PT. HURICANE INTERNATIONAL', phoneNo: '021 365364', mobileNo: '021 365364', faxNo: '021 365364', email: 'cs@hrcb.com', address: 'Jl. Menteng Raya No. 76', city: 'Jakarta Pusat' },
  { id: 3, propertyName: 'HURICANE APARTMENT', company: 'PT. HURICANE INTERNATIONAL', phoneNo: '012 87168768', mobileNo: '012 87168768', faxNo: '012 87168768', email: 'cs@hrcs.com', address: 'Jl. Kyai Tapa No.65', city: 'Jakarta Barat' },
];
const columns = [
  {
    name: 'Property Name',
    selector: 'propertyName',
    sortable: true,
  },
  {
    name: 'Company',
    selector: 'company',
    sortable: true,
  },
  {
    name: 'Phone No',
    selector: 'phoneNo',
    sortable: true,
  },
  {
    name: 'Mobile No',
    selector: 'mobileNo',
    sortable: true,
  },
  {
    name: 'Fax No',
    selector: 'faxNo',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'City',
    selector: 'city',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class CompanyProperties extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Company Properties" />
            <DataTable
              title="Data Company Properties"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default CompanyProperties;
