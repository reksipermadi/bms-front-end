import React, { Component } from 'react';
import { Card,  CardBody,Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailCompanyProperties extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Company Properties" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="3">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <Label>Company Logo</Label>
                          <FormText className="help-block"></FormText>
                        </FormGroup>
                      </Col>
                      <hr/>
                      <Col md="12">
                        <FormGroup>
                          <Label>Property Documents</Label>
                          <FormText className="help-block"></FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Col>
                  <Col md="9">
                    <Row>
                      <Col md="4">
                        <FormGroup>
                          <Label>Company Name</Label>
                          <FormText className="help-block">PT. HURICANE INTERNATIONAL</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <Label>Property Code</Label>
                          <FormText className="help-block">HRCA</FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                    <hr/>
                    <Row>
                      <Col md="4">
                        <FormGroup>
                          <Label>Property Name</Label>
                          <FormText className="help-block">HURICANE APARTMENT</FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                    <hr/>
                    <Row>
                      <Col md="4">
                        <FormGroup>
                          <Label>Phone Number</Label>
                          <FormText className="help-block">021-8934094</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <Label>Fax Number</Label>
                          <FormText className="help-block">021-8934095</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <Label>Website</Label>
                          <FormText className="help-block">WWW.HURICANE.COM</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <Label>Mobile Number</Label>
                          <FormText className="help-block">083472349234</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="4">
                        <FormGroup>
                          <Label>Email</Label>
                          <FormText className="help-block">CS@HURICANE.COM</FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                    <hr/>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <Label>Address</Label>
                          <FormText className="help-block">Jl. Bambu Apus No.089</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Country</Label>
                          <FormText className="help-block">Indonesia</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Province</Label>
                          <FormText className="help-block">DKI Jakarta</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>City</Label>
                          <FormText className="help-block">Jakarta Barat</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Postal Code</Label>
                          <FormText className="help-block">16782</FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                    <hr/>
                    <Row>
                      <Col md="4">
                        <FormGroup>
                          <Label>Description</Label>
                          <FormText className="help-block"></FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailCompanyProperties;
