import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddOwnerTenant extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Owner & Tenant" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Owner & Tenant</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label>Contact Type</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="owner" name="contactType" value="owner" />
                        <Label className="form-check-label" check htmlFor="owner">OWNER</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="tenant" name="contactType" value="tenant" />
                        <Label className="form-check-label" check htmlFor="tenant">Tenant</Label>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="company">Company</Label>
                        <Input type="text" id="company" name="company" placeholder="Enter company.." />
                        <FormText className="help-block">Please enter company</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="contactName">Contact Name</Label>
                        <Input type="text" id="contactName" name="contactName" placeholder="Enter contact name.." />
                        <FormText className="help-block">Please enter contact name</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="identityType">Identity Type</Label>
                        <Input type="select" name="identityType" id="identityType">
                          <option value="0">Choose Type</option>
                          <option value="1">KTP</option>
                          <option value="2">PASPOR</option>
                          <option value="3">SIM</option>
                        </Input>
                        <FormText className="help-block">Please select type</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="identityNo">Identity No</Label>
                        <Input type="text" id="identityNo" name="identityNo" placeholder="Enter identity no.." />
                        <FormText className="help-block">Please enter identity no</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="placeOfBirth">Place of Birth</Label>
                        <Input type="text" id="placeOfBirth" name="placeOfBirth" placeholder="Enter place of birth.." />
                        <FormText className="help-block">Please enter place of birth</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="dateOfBirth">Date of Birth</Label>
                        <InputGroup>  
                          <Input type="date" id="dateOfBirth" name="dateOfBirth" placeholder="Enter date of birth.." />
                          <InputGroupAddon addonType="append">
                            <Button type="button" color="default"><i class="fa fa-calendar"></i></Button>
                          </InputGroupAddon>
                        </InputGroup>
                        <FormText className="help-block">Please enter Enter date of birth</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="gender">Gender</Label>
                        <Input type="select" name="gender" id="gender">
                          <option value="0">Choose Gender</option>
                          <option value="1">Male</option>
                          <option value="2">Female</option>
                        </Input>
                        <FormText className="help-block">Please select gender</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="religion">Religion</Label>
                        <Input type="select" name="religion" id="religion">
                          <option value="0">Choose Gender</option>
                          <option value="1">Islam</option>
                          <option value="2">Kristen</option>
                          <option value="3">Katolik</option>
                          <option value="4">Hindu</option>
                          <option value="5">Budha</option>
                        </Input>
                        <FormText className="help-block">Please select Religion</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="maritialStatus">Maritial Status</Label>
                        <Input type="select" name="maritialStatus" id="maritialStatus">
                          <option value="0">Choose Maritial Status</option>
                          <option value="1">Single</option>
                          <option value="2">Maried</option>
                          <option value="3">Divorced</option>
                        </Input>
                        <FormText className="help-block">Please select maritial status</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="job">Job / Profession</Label>
                        <Input type="text" id="job" name="job" placeholder="Enter job / profession.." />
                        <FormText className="help-block">Please enter job / profession</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="phoneNumber">Phone Number</Label>
                        <Input type="text" id="phoneNumber" name="phoneNumber" placeholder="Enter phone number.." />
                        <FormText className="help-block">Please enter phone number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="faxNumber">Fax Number</Label>
                        <Input type="text" id="faxNumber" name="faxNumber" placeholder="Enter fax number.." />
                        <FormText className="help-block">Please enter fax number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="website">Website</Label>
                        <Input type="text" id="website" name="website" placeholder="Enter website.." />
                        <FormText className="help-block">Please enter website</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="mobileNumber">Mobile Number</Label>
                        <Input type="text" id="mobileNumber" name="mobileNumber" placeholder="Enter mobile number.." />
                        <FormText className="help-block">Please enter mobile number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label htmlFor="address">Address</Label>
                        <Input type="textarea" id="address" name="address" placeholder="Enter address.." />
                        <FormText className="help-block">Please enter address</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="country">Country</Label>
                        <Input type="select" name="country" id="country">
                          <option value="0">Choose Country</option>
                          <option value="1">ID - Indonesia</option>
                        </Input>
                        <FormText className="help-block">Please select country</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="province">Province</Label>
                        <Input type="select" name="province" id="province">
                          <option value="0">Choose Province</option>
                        </Input>
                        <FormText className="help-block">Please select province</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="city">City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Choose City</option>
                        </Input>
                        <FormText className="help-block">Please select city</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="postalCode">Postal Code</Label>
                        <Input type="text" id="postalCode" name="postalCode" placeholder="Enter postalcode.." />
                        <FormText className="help-block">Please enter postal code</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="vehicleNo">Vehicle No</Label>
                        <Input type="text" id="vehicleNo" name="vehicleNo" placeholder="Enter vehicle no.." />
                        <FormText className="help-block">Please enter vehicle no</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="memberOfContact">Member of Contact</Label>
                        <Input type="text" id="memberOfContact" name="memberOfContact" placeholder="Enter member of contact.." />
                        <FormText className="help-block">Please enter member of contact</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="photoProfile">Photo Profile</Label>
                        <Input type="file" id="photoProfile" name="photoProfile" placeholder="Enter photo profile.." />
                        <FormText className="help-block">Please enter photo profile</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="documents">Documents</Label>
                        <Input type="file" id="documents" name="documents" placeholder="Enter documents.." />
                        <FormText className="help-block">Please enter documents</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddOwnerTenant;
