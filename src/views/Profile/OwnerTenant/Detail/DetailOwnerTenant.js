import React, { Component } from 'react';
import { Card,  CardBody,Label, FormGroup, Button, FormText, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class DetailOwnerTenant extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Detail" title="Owner Tenant" />
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <div className="btn-holder mb-3">
                  <Button color="primary" className="mr-2"><i class="fa fa-arrow-left"></i> Back</Button>
                  <Button color="success"><i class="fa fa-edit"></i> Edit</Button>
                </div>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Contact Type</Label>
                      <FormText className="help-block">OWNER</FormText>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Company</Label>
                      <FormText className="help-block">PT. BMS</FormText>
                    </FormGroup>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Photo Profile</Label>
                      <FormText className="help-block"></FormText>
                    </FormGroup>
                  </Col>
                  <Col md="9">
                    <Row>
                      <Col md="3">
                        <FormGroup>
                          <Label>Contact Name</Label>
                          <FormText className="help-block">REZA</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Identity Type</Label>
                          <FormText className="help-block">KTP</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Identity No</Label>
                          <FormText className="help-block">1291291290334</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Place of Birth</Label>
                          <FormText className="help-block">Jakarta</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Date of Birth</Label>
                          <FormText className="help-block">09 Feb 1967</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Gender</Label>
                          <FormText className="help-block">Male</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Religion</Label>
                          <FormText className="help-block">Islam</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Maritial Status</Label>
                          <FormText className="help-block">Married</FormText>
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup>
                          <Label>Job / Profession</Label>
                          <FormText className="help-block">Manager</FormText>
                        </FormGroup>
                      </Col>
                    </Row>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label>Document</Label>
                        <FormText className="help-block"></FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label>Phone Number</Label>
                        <FormText className="help-block">021-8934094</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Fax Number</Label>
                        <FormText className="help-block">021-8934095</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Website</Label>
                        <FormText className="help-block">WWW.HURICANE.COM</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Mobile Number</Label>
                        <FormText className="help-block">083472349234</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Email</Label>
                        <FormText className="help-block">REZA@GMAIL.COM</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label>Address</Label>
                        <FormText className="help-block">JL. PASIR PUTIH NO.26 ANCOL</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Country</Label>
                        <FormText className="help-block">Indonesia</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Province</Label>
                        <FormText className="help-block">Banten</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>City</Label>
                        <FormText className="help-block">Tangerang Kota</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Postal Code</Label>
                        <FormText className="help-block">16782</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="3">
                      <FormGroup>
                        <Label>Vehicle No</Label>
                        <FormText className="help-block">B 12345 KCT</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Member of Contact</Label>
                        <FormText className="help-block"></FormText>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>Description</Label>
                        <FormText className="help-block"></FormText>
                      </FormGroup>
                    </Col>
                  </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetailOwnerTenant;
