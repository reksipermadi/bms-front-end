import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, type: 'OWNER', company: 'PT. BMS', contactName: 'REZA', phoneNo: '021-3987261', mobileNo: '081287657898', faxNo: '', email: 'REZA@GMAIL.COM', address: 'JL. PASIR PUTIH NO.26 ANCOL', city: 'Jakarta Pusat' },
  { id: 2, type: 'OWNER', company: 'PT Maju Mundur', contactName: 'Eko Patrio', phoneNo: '0897998998', mobileNo: '8989899899', faxNo: '', email: 'EKO@GMAIL.COM', address: 'Jalan Maju Muncur', city: 'Jakarta Barat' },
  { id: 3, type: 'TENANT', company: 'PT Kentanix Indonesia', contactName: 'Sheeza Ramadhina Khadafi', phoneNo: '089681887297', mobileNo: '', faxNo: '', email: 'SHEEZA@GMAIL.COM', address: 'Grand Nusa Indah', city: 'Jakarta Pusat' },
];
const columns = [
  {
    name: 'Type',
    selector: 'type',
    sortable: true,
  },
  {
    name: 'Company',
    selector: 'company',
    sortable: true,
  },
  {
    name: 'Contact Name',
    selector: 'contactName',
    sortable: true,
  },
  {
    name: 'Phone No',
    selector: 'phoneNo',
    sortable: true,
  },
  {
    name: 'Mobile No',
    selector: 'mobileNo',
    sortable: true,
  },
  {
    name: 'Fax No',
    selector: 'faxNo',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'City',
    selector: 'city',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class OwnerTenant extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Owner & Tenant" />
            <DataTable
              title="Data Owner & Tenant"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default OwnerTenant;
