import React, { Component } from 'react';
import { Input, Button, Card, CardBody } from 'reactstrap';
import HeadTitle from '../../Shared/HeadTitle/HeadTitle';
import DataTable from 'react-data-table-component';

const data = [
  { id: 1, companyType: 'HEAD OFFICE', propertyType: 'SUPERBLOCK', companyName: 'PT. HURICANE INTERNATIONAL', phoneNo: '021-8934094', mobileNo: '083472349234', faxNo: '021-8934095', email: 'CS@HURICANE.COM', city: 'Tangerang Kota' },
];
const columns = [
  {
    name: 'Company Type',
    selector: 'companyType',
    sortable: true,
  },
  {
    name: 'Property Type',
    selector: 'propertyType',
    sortable: true,
  },
  {
    name: 'Company Name',
    selector: 'companyName',
    sortable: true,
  },
  {
    name: 'Phone No',
    selector: 'phoneNo',
    sortable: true,
  },
  {
    name: 'Mobile No',
    selector: 'mobileNo',
    sortable: true,
  },
  {
    name: 'Fax No',
    selector: 'faxNo',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'City',
    selector: 'city',
    sortable: true,
  },
  {
    name: 'Action',
    cell: () => <Button color="primary"><i class="fa fa-eye"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="success"><i class="fa fa-edit"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
  {
    cell: () => <Button color="danger"><i class="fa fa-trash"></i></Button>,
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
    width: '56px',
  },
];

const actions = (
  <div>
    <Button className="mr-2" color="success">
      <i class="fa fa-plus fa-lg"></i> New
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-download fa-lg"></i> Export
    </Button>
    <Button className="mr-2" color="primary">
      <i class="fa fa-upload fa-lg"></i> Import
    </Button>
  </div>
);

class Company extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <HeadTitle title="Company" />
            <DataTable
              title="Data Company"
              columns={columns}
              data={data}
              actions={actions}
              subHeader
              subHeaderComponent={
                (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Input type="text" name="Search" placeholder="Search" />
                  </div>
                )
              }
              pagination
              highlightOnHover
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Company;
