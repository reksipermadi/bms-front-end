import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Input, Label, FormGroup, Button, Form, FormText, CardFooter, Row, Col } from 'reactstrap';
import HeadTitle from '../../../Shared/HeadTitle/HeadTitle';

class AddCompany extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <HeadTitle action="Add" title="Company" />
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <strong>Add Company</strong> Form
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label>Company Type</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="headOffice" name="companyType" value="headOffice" />
                        <Label className="form-check-label" check htmlFor="headOffice">HEAD OFFICE</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="branch" name="companyType" value="branch" />
                        <Label className="form-check-label" check htmlFor="branch">Branch</Label>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="propertyType">Property Type</Label>
                        <Input type="select" name="propertyType" id="propertyType">
                          <option value="0">Choose Property Type</option>
                          <option value="1">SUPERBLOCK</option>
                          <option value="2">APARTMEN</option>
                        </Input>
                        <FormText className="help-block">Please select property type</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="companyName">Company Name</Label>
                        <Input type="text" id="companyName" name="companyName" placeholder="Enter company name.." />
                        <FormText className="help-block">Please enter company name</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="taxIdNumber">Tax ID Number</Label>
                        <Input type="text" id="taxIdNumber" name="taxIdNumber" placeholder="Enter tax id number.." />
                        <FormText className="help-block">Please enter tax id number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="mobileNumber">Mobile Number</Label>
                        <Input type="text" id="mobileNumber" name="mobileNumber" placeholder="Enter mobile number.." />
                        <FormText className="help-block">Please enter mobile number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Input type="text" id="email" name="email" placeholder="Enter email.." />
                        <FormText className="help-block">Please enter email</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="phoneNumber">Phone Number</Label>
                        <Input type="text" id="phoneNumber" name="phoneNumber" placeholder="Enter phone number.." />
                        <FormText className="help-block">Please enter phone number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="faxNumber">Fax Number</Label>
                        <Input type="text" id="faxNumber" name="faxNumber" placeholder="Enter fax number.." />
                        <FormText className="help-block">Please enter fax number</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="website">Website</Label>
                        <Input type="text" id="website" name="website" placeholder="Enter website.." />
                        <FormText className="help-block">Please enter website</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="12">
                      <FormGroup>
                        <Label htmlFor="address">Address</Label>
                        <Input type="textarea" id="address" name="address" placeholder="Enter address.." />
                        <FormText className="help-block">Please enter address</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="country">Country</Label>
                        <Input type="select" name="country" id="country">
                          <option value="0">Choose Country</option>
                          <option value="1">ID - Indonesia</option>
                        </Input>
                        <FormText className="help-block">Please select country</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="province">Province</Label>
                        <Input type="select" name="province" id="province">
                          <option value="0">Choose Province</option>
                        </Input>
                        <FormText className="help-block">Please select province</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="city">City</Label>
                        <Input type="select" name="city" id="city">
                          <option value="0">Choose City</option>
                        </Input>
                        <FormText className="help-block">Please select city</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="postalCode">Postal Code</Label>
                        <Input type="text" id="postalCode" name="postalCode" placeholder="Enter postalcode.." />
                        <FormText className="help-block">Please enter postal code</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="8">
                      <FormGroup>
                        <Label htmlFor="description">Description</Label>
                        <Input type="textarea" id="description" name="description" placeholder="Enter description.." />
                        <FormText className="help-block">Please enter description</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="companyLogo">Company Logo</Label>
                        <Input type="file" id="companyLogo" name="companyLogo" placeholder="Enter company logo.." />
                        <FormText className="help-block">Please enter company logo</FormText>
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="documents">Documents</Label>
                        <Input type="file" id="documents" name="documents" placeholder="Enter documents.." />
                        <FormText className="help-block">Please enter documents</FormText>
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Cancel</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AddCompany;
