import React, { Component } from 'react';

class HeadTitle extends Component {
  render() {

    const { title, action } = this.props

    return (
      <div className="bms-head-title">
        <span className="display-4">{action} <b>{title}</b></span>
      </div>
    );
  }
}

export default HeadTitle;
