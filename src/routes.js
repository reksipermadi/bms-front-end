import React from 'react';

// const Login = React.lazy(() => import('./views/Login'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const AppConfig = React.lazy(() => import('./views/Settings/AppConfig'));
const Province = React.lazy(() => import('./views/Settings/MasterData/Province'));
const AddProvince = React.lazy(() => import('./views/Settings/MasterData/Province/Add/AddProvince'));
const DetailProvince = React.lazy(() => import('./views/Settings/MasterData/Province/Detail/DetailProvince'));
const City = React.lazy(() => import('./views/Settings/MasterData/City'));
const AddCity = React.lazy(() => import('./views/Settings/MasterData/City/Add/AddCity'));
const DetailCity = React.lazy(() => import('./views/Settings/MasterData/City/Detail/DetailCity'));
const Country = React.lazy(() => import('./views/Settings/MasterData/Country'));
const AddCountry = React.lazy(() => import('./views/Settings/MasterData/Country/Add/AddCountry'));
const DetailCountry = React.lazy(() => import('./views/Settings/MasterData/Country/Detail/DetailCountry'));
const ChartOfAccount = React.lazy(() => import('./views/GeneralSetup/Account/ChartOfAccount/ChartOfAccount'));
const AddChartOfAccount = React.lazy(() => import('./views/GeneralSetup/Account/ChartOfAccount/Add/AddChartOfAccount'));
const DetailChartOfAccount = React.lazy(() => import('./views/GeneralSetup/Account/ChartOfAccount/Detail/DetailChartOfAccount'));
const BankAccount = React.lazy(() => import('./views/GeneralSetup/Account/BankAccount/BankAccount'));
const AddBankAccount = React.lazy(() => import('./views/GeneralSetup/Account/BankAccount/Add/AddBankAccount'));
const DetailBankAccount = React.lazy(() => import('./views/GeneralSetup/Account/BankAccount/Detail/DetailBankAccount'));
const OpeningBalance = React.lazy(() => import('./views/GeneralSetup/Account/OpeningBalance/OpeningBalance'));
const BudgetGroup = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetGroup/BudgetGroup'));
const AddBudgetGroup = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetGroup/Add/AddBudgetGroup'));
const DetailBudgetGroup = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetGroup/Detail/DetailBudgetGroup'));
const BudgetSettings = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetSettings/BudgetSettings'));
const AddBudgetSettings = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetSettings/Add/AddBudgetSettings'));
const DetailBudgetSettings = React.lazy(() => import('./views/GeneralSetup/Budget/BudgetSettings/Detail/DetailBudgetSettings'));
const ComplaintCategory = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintCategory/ComplaintCategory'));
const AddComplaintCategory = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintCategory/Add/AddComplaintCategory'));
const DetailComplaintCategory = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintCategory/Detail/DetailComplaintCategory'));
const ComplaintIssue = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintIssue/ComplaintIssue'));
const AddComplaintIssue = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintIssue/Add/AddComplaintIssue'));
const DetailComplaintIssue = React.lazy(() => import('./views/GeneralSetup/Complaint/ComplaintIssue/Detail/DetailComplaintIssue'));
const Division = React.lazy(() => import('./views/GeneralSetup/Organization/Division/Division'));
const AddDivision = React.lazy(() => import('./views/GeneralSetup/Organization/Division/Add/AddDivision'));
const DetailDivision = React.lazy(() => import('./views/GeneralSetup/Organization/Division/Detail/DetailDivision'));
const Department = React.lazy(() => import('./views/GeneralSetup/Organization/Department/Department'));
const AddDepartment = React.lazy(() => import('./views/GeneralSetup/Organization/Department/Add/AddDepartment'));
const DetailDepartment = React.lazy(() => import('./views/GeneralSetup/Organization/Department/Detail/DetailDepartment'));
const InvoiceGroup = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceGroup/InvoiceGroup'));
const AddInvoiceGroup = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceGroup/Add/AddInvoiceGroup'));
const DetailInvoiceGroup = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceGroup/Detail/DetailInvoiceGroup'));
const InvoiceCategory = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceCategory/InvoiceCategory'));
const AddInvoiceCategory = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceCategory/Add/AddInvoiceCategory'));
const DetailInvoiceCategory = React.lazy(() => import('./views/GeneralSetup/Invoice/InvoiceCategory/Detail/DetailInvoiceCategory'));
const ServiceGroup = React.lazy(() => import('./views/GeneralSetup/Service/ServiceGroup/ServiceGroup'));
const AddServiceGroup = React.lazy(() => import('./views/GeneralSetup/Service/ServiceGroup/Add/AddServiceGroup'));
const DetailServiceGroup = React.lazy(() => import('./views/GeneralSetup/Service/ServiceGroup/Detail/DetailServiceGroup'));
const ServiceSetting = React.lazy(() => import('./views/GeneralSetup/Service/ServiceSetting/ServiceSetting'));
const AddServiceSetting = React.lazy(() => import('./views/GeneralSetup/Service/ServiceSetting/Add/AddServiceSetting'));
const DetailServiceSetting = React.lazy(() => import('./views/GeneralSetup/Service/ServiceSetting/Detail/DetailServiceSetting'));
const MeterGroup = React.lazy(() => import('./views/GeneralSetup/Meter/MeterGroup/MeterGroup'));
const AddMeterGroup = React.lazy(() => import('./views/GeneralSetup/Meter/MeterGroup/Add/AddMeterGroup'));
const DetailMeterGroup = React.lazy(() => import('./views/GeneralSetup/Meter/MeterGroup/Detail/DetailMeterGroup'));
const Meter = React.lazy(() => import('./views/GeneralSetup/Meter/Meter'));
const AddMeter = React.lazy(() => import('./views/GeneralSetup/Meter/Add/AddMeter'));
const DetailMeter = React.lazy(() => import('./views/GeneralSetup/Meter/Detail/DetailMeter'));
const MeterMapping = React.lazy(() => import('./views/GeneralSetup/Meter/MeterMapping/MeterMapping'));
const AddMeterMapping = React.lazy(() => import('./views/GeneralSetup/Meter/MeterMapping/Add/AddMeterMapping'));
const DetailMeterMapping = React.lazy(() => import('./views/GeneralSetup/Meter/MeterMapping/Detail/DetailMeterMapping'));
const SupplierGroup = React.lazy(() => import('./views/GeneralSetup/Supplier/SupplierGroup/SupplierGroup'));
const AddSupplierGroup = React.lazy(() => import('./views/GeneralSetup/Supplier/SupplierGroup/Add/AddSupplierGroup'));
const DetailSupplierGroup = React.lazy(() => import('./views/GeneralSetup/Supplier/SupplierGroup/Detail/DetailSupplierGroup'));
const Supplier = React.lazy(() => import('./views/GeneralSetup/Supplier/Supplier/Supplier'));
const AddSupplier = React.lazy(() => import('./views/GeneralSetup/Supplier/Supplier/Add/AddSupplier'));
const DetailSupplier = React.lazy(() => import('./views/GeneralSetup/Supplier/Supplier/Detail/DetailSupplier'));
const UserAccount = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserAccount/UserAccount'));
const AddUserAccount = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserAccount/Add/AddUserAccount'));
const DetailUserAccount = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserAccount/Detail/DetailUserAccount'));
const UserRoles = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserRoles/UserRoles'));
const AddUserRoles = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserRoles/Add/AddUserRoles'));
const DetailUserRoles = React.lazy(() => import('./views/GeneralSetup/UserManagement/UserRoles/Detail/DetailUserRoles'));
const DocNumberFormat = React.lazy(() => import('./views/GeneralSetup/DocNumberFormat/DocNumberFormat'));
const AddDocNumberFormat = React.lazy(() => import('./views/GeneralSetup/DocNumberFormat/Add/AddDocNumberFormat'));
const DetailDocNumberFormat = React.lazy(() => import('./views/GeneralSetup/DocNumberFormat/Detail/DetailDocNumberFormat'));
const TaxSetting = React.lazy(() => import('./views/GeneralSetup/TaxSetting/TaxSetting'));
const AddTaxSetting = React.lazy(() => import('./views/GeneralSetup/TaxSetting/Add/AddTaxSetting'));
const DetailTaxSetting = React.lazy(() => import('./views/GeneralSetup/TaxSetting/Detail/DetailTaxSetting'));
const Warehouse = React.lazy(() => import('./views/GeneralSetup/Warehouse/Warehouse'));
const AddWarehouse = React.lazy(() => import('./views/GeneralSetup/Warehouse/Add/AddWarehouse'));
const DetailWarehouse = React.lazy(() => import('./views/GeneralSetup/Warehouse/Detail/DetailWarehouse'));
const LeasableArea = React.lazy(() => import('./views/GeneralSetup/LeasableArea/LeasableArea'));
const AddLeasableArea = React.lazy(() => import('./views/GeneralSetup/LeasableArea/Add/AddLeasableArea'));
const DetailLeasableArea = React.lazy(() => import('./views/GeneralSetup/LeasableArea/Detail/DetailLeasableArea'));
const Brand = React.lazy(() => import('./views/Items/Equipment/Brand/Brand'));
const AddBrand = React.lazy(() => import('./views/Items/Equipment/Brand/Add/AddBrand'));
const DetailBrand = React.lazy(() => import('./views/Items/Equipment/Brand/Detail/DetailBrand'));
const EquipmentCategory = React.lazy(() => import('./views/Items/Equipment/EquipmentCategory/EquipmentCategory'));
const AddEquipmentCategory = React.lazy(() => import('./views/Items/Equipment/EquipmentCategory/Add/AddEquipmentCategory'));
const DetailEquipmentCategory = React.lazy(() => import('./views/Items/Equipment/EquipmentCategory/Detail/DetailEquipmentCategory'));
const Equipment = React.lazy(() => import('./views/Items/Equipment/Equipment/Equipment'));
const AddEquipment = React.lazy(() => import('./views/Items/Equipment/Equipment/Add/AddEquipment'));
const DetailEquipment = React.lazy(() => import('./views/Items/Equipment/Equipment/Detail/DetailEquipment'));
const MaterialCategory = React.lazy(() => import('./views/Items/Material/MaterialCategory/MaterialCategory'));
const AddMaterialCategory = React.lazy(() => import('./views/Items/Material/MaterialCategory/Add/AddMaterialCategory'));
const DetailMaterialCategory = React.lazy(() => import('./views/Items/Material/MaterialCategory/Detail/DetailMaterialCategory'));
const Material = React.lazy(() => import('./views/Items/Material/Material/Material'));
const AddMaterial = React.lazy(() => import('./views/Items/Material/Material/Add/AddMaterial'));
const DetailMaterial = React.lazy(() => import('./views/Items/Material/Material/Detail/DetailMaterial'));
const Company = React.lazy(() => import('./views/Profile/Company/Company'));
const AddCompany = React.lazy(() => import('./views/Profile/Company/Add/AddCompany'));
const DetailCompany = React.lazy(() => import('./views/Profile/Company/Detail/DetailCompany'));
const CompanyProperties = React.lazy(() => import('./views/Profile/CompanyProperties/CompanyProperties'));
const AddCompanyProperties = React.lazy(() => import('./views/Profile/CompanyProperties/Add/AddCompanyProperties'));
const DetailCompanyProperties = React.lazy(() => import('./views/Profile/CompanyProperties/Detail/DetailCompanyProperties'));
const CompanyUnits = React.lazy(() => import('./views/Profile/CompanyUnits/CompanyUnits'));
const AddCompanyUnits = React.lazy(() => import('./views/Profile/CompanyUnits/Add/AddCompanyUnits'));
const DetailCompanyUnits = React.lazy(() => import('./views/Profile/CompanyUnits/Detail/DetailCompanyUnits'));
const OwnerTenant = React.lazy(() => import('./views/Profile/OwnerTenant/OwnerTenant'));
const AddOwnerTenant = React.lazy(() => import('./views/Profile/OwnerTenant/Add/AddOwnerTenant'));
const DetailOwnerTenant = React.lazy(() => import('./views/Profile/OwnerTenant/Detail/DetailOwnerTenant'));
const UnitRent = React.lazy(() => import('./views/Tenancy/UnitRent/UnitRent'));
const DetailUnitRent = React.lazy(() => import('./views/Tenancy/UnitRent/Detail/DetailUnitRent'));
const Maintenance = React.lazy(() => import('./views/Maintenances/Maintenance/Maintenance'));
const AddMaintenance = React.lazy(() => import('./views/Maintenances/Maintenance/Add/AddMaintenance'));
const DetailMaintenance = React.lazy(() => import('./views/Maintenances/Maintenance/Detail/DetailMaintenance'));
const Billings = React.lazy(() => import('./views/Transactions/Billings/Billings'));
const DetailBillings = React.lazy(() => import('./views/Transactions/Billings/Detail/DetailBillings'));
const Invoices = React.lazy(() => import('./views/Transactions/Invoices/Invoices'));
const DetailInvoices = React.lazy(() => import('./views/Transactions/Invoices/Detail/DetailInvoices'));
const HistoryInvoices = React.lazy(() => import('./views/Transactions/Invoices/History/HistoryInvoices'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/settings/appconfig', name: 'App Config', component: AppConfig },
  { path: '/masterdata/province', name: 'Province', component: Province },
  { path: '/masterdata/add-province', name: 'Add Province', component: AddProvince },
  { path: '/masterdata/detail-province', name: 'Detail Province', component: DetailProvince },
  { path: '/masterdata/city', name: 'City', component: City },
  { path: '/masterdata/add-city', name: 'Add City', component: AddCity },
  { path: '/masterdata/detail-city', name: 'Detail City', component: DetailCity },
  { path: '/masterdata/country', name: 'Country', component: Country },
  { path: '/masterdata/add-country', name: 'Add Country', component: AddCountry },
  { path: '/masterdata/detail-country', name: 'Detail Country', component: DetailCountry },
  { path: '/account/chart-of-account', name: 'Chart Of Account', component: ChartOfAccount },
  { path: '/account/add-chart-of-account', name: 'Add Chart Of Account', component: AddChartOfAccount },
  { path: '/account/detail-chart-of-account', name: 'Detail Chart Of Account', component: DetailChartOfAccount },
  { path: '/account/bank-account', name: 'Bank Account', component: BankAccount },
  { path: '/account/add-bank-account', name: 'Add Bank Account', component: AddBankAccount },
  { path: '/account/detail-bank-account', name: 'Detail Bank Account', component: DetailBankAccount },
  { path: '/account/opening-balance', name: 'Detail Bank Account', component: OpeningBalance },
  { path: '/budget/budget-group', name: 'Budget Group', component: BudgetGroup },
  { path: '/budget/add-budget-group', name: 'Add Budget Group', component: AddBudgetGroup },
  { path: '/budget/detail-budget-group', name: 'Detail Budget Group', component: DetailBudgetGroup },
  { path: '/budget/budget-settings', name: 'Budget Settings', component: BudgetSettings },
  { path: '/budget/add-budget-settings', name: 'Add Budget Settings', component: AddBudgetSettings },
  { path: '/budget/detail-budget-settings', name: 'Detail Budget Settings', component: DetailBudgetSettings },
  { path: '/complaint/complaint-category', name: 'Complaint Category', component: ComplaintCategory },
  { path: '/complaint/add-complaint-category', name: 'Add Complaint Category', component: AddComplaintCategory },
  { path: '/complaint/detail-complaint-category', name: 'Detail Complaint Category', component: DetailComplaintCategory },
  { path: '/complaint/complaint-issue', name: 'Complaint Issue', component: ComplaintIssue },
  { path: '/complaint/add-complaint-issue', name: 'Add Complaint Issue', component: AddComplaintIssue },
  { path: '/complaint/detail-complaint-issue', name: 'Detail Complaint Issue', component: DetailComplaintIssue },
  { path: '/organization/division', name: 'Division', component: Division },
  { path: '/organization/add-division', name: 'Add Division', component: AddDivision },
  { path: '/organization/detail-division', name: 'Detail Division', component: DetailDivision },
  { path: '/organization/department', name: 'Department', component: Department },
  { path: '/organization/add-department', name: 'Add Department', component: AddDepartment },
  { path: '/organization/detail-department', name: 'Detail Department', component: DetailDepartment },
  { path: '/invoice/invoice-group', name: 'Invoice Group', component: InvoiceGroup },
  { path: '/invoice/add-invoice-group', name: 'Add Invoice Group', component: AddInvoiceGroup },
  { path: '/invoice/detail-invoice-group', name: 'Detail Invoice Group', component: DetailInvoiceGroup },
  { path: '/invoice/invoice-category', name: 'Invoice Category', component: InvoiceCategory },
  { path: '/invoice/add-invoice-category', name: 'Add Invoice Category', component: AddInvoiceCategory },
  { path: '/invoice/detail-invoice-category', name: 'Detail Invoice Category', component: DetailInvoiceCategory },
  { path: '/service/service-group', name: 'Service Group', component: ServiceGroup },
  { path: '/service/add-service-group', name: 'Add Service Group', component: AddServiceGroup },
  { path: '/service/detail-service-group', name: 'Detail Service Group', component: DetailServiceGroup },
  { path: '/service/service-setting', name: 'Service Setting', component: ServiceSetting },
  { path: '/service/add-service-setting', name: 'Add Service Setting', component: AddServiceSetting },
  { path: '/service/detail-service-setting', name: 'Detail Service Setting', component: DetailServiceSetting },
  { path: '/meter/', name: 'Meter', component: Meter },
  { path: '/add-meter', name: 'Add Meter', component: AddMeter },
  { path: '/detail-meter', name: 'Detail Meter', component: DetailMeter },
  { path: '/meters/meter-group', name: 'Meter Group', component: MeterGroup },
  { path: '/meters/add-meter-group', name: 'Add Meter Group', component: AddMeterGroup },
  { path: '/meters/detail-meter-group', name: 'Detail Meter Group', component: DetailMeterGroup },
  { path: '/meters/meter-mapping', name: 'Meter Mapping', component: MeterMapping },
  { path: '/meters/add-meter-mapping', name: 'Add Meter Mapping', component: AddMeterMapping },
  { path: '/meters/detail-meter-mapping', name: 'Detail Meter Mapping', component: DetailMeterMapping },
  { path: '/suppliers/supplier-group', name: 'Supplier Group', component: SupplierGroup },
  { path: '/suppliers/add-supplier-group', name: 'Add Supplier Group', component: AddSupplierGroup },
  { path: '/suppliers/detail-supplier-group', name: 'Detail Supplier Group', component: DetailSupplierGroup },
  { path: '/supplier', name: 'Supplier', component: Supplier },
  { path: '/add-supplier', name: 'Add Supplier', component: AddSupplier },
  { path: '/detail-supplier', name: 'Detail Supplier', component: DetailSupplier },
  { path: '/user-management/user-account', name: 'User Account', component: UserAccount },
  { path: '/user-management/add-user-account', name: 'Add User Account', component: AddUserAccount },
  { path: '/user-management/detail-user-account', name: 'Detail User Account', component: DetailUserAccount },
  { path: '/user-management/user-roles', name: 'User Roles', component: UserRoles },
  { path: '/user-management/add-user-roles', name: 'Add User Roles', component: AddUserRoles },
  { path: '/user-management/detail-user-roles', name: 'Detail User Roles', component: DetailUserRoles },
  { path: '/doc-number-format', name: 'Doc Number Format', component: DocNumberFormat },
  { path: '/add-doc-number-format', name: 'Add Doc Number Format', component: AddDocNumberFormat },
  { path: '/detail-doc-number-format', name: 'Detail Doc Number Format', component: DetailDocNumberFormat },
  { path: '/tax-settings', name: 'Tax Setting', component: TaxSetting },
  { path: '/add-tax-settings', name: 'Add Tax Setting', component: AddTaxSetting },
  { path: '/detail-tax-settings', name: 'Detail Tax Setting', component: DetailTaxSetting },
  { path: '/warehouse', name: 'Warehouse', component: Warehouse },
  { path: '/add-warehouse', name: 'Add Warehouse', component: AddWarehouse },
  { path: '/detail-warehouse', name: 'Detail Warehouse', component: DetailWarehouse },
  { path: '/leasable-area', name: 'Leasable Area', component: LeasableArea },
  { path: '/add-leasable-area', name: 'Add Leasable Area', component: AddLeasableArea },
  { path: '/detail-leasable-area', name: 'Detail Leasable Area', component: DetailLeasableArea },
  { path: '/equipments/brands', name: 'Brand', component: Brand },
  { path: '/equipments/add-brands', name: 'Add Brand', component: AddBrand },
  { path: '/equipments/detail-brands', name: 'Detail Brand', component: DetailBrand },
  { path: '/equipments/equipment-category', name: 'Equipment Category', component: EquipmentCategory },
  { path: '/equipments/add-equipment-category', name: 'Add Equipment Category', component: AddEquipmentCategory },
  { path: '/equipments/detail-equipment-category', name: 'Detail Equipment Category', component: DetailEquipmentCategory },
  { path: '/equipment', name: 'Equipment', component: Equipment },
  { path: '/add-equipment', name: 'Add Equipment', component: AddEquipment },
  { path: '/detail-equipment', name: 'Detail Equipment', component: DetailEquipment },
  { path: '/materials/material-category', name: 'Material Category', component: MaterialCategory },
  { path: '/materials/add-material-category', name: 'Add Material Category', component: AddMaterialCategory },
  { path: '/materials/detail-material-category', name: 'Detail Material Category', component: DetailMaterialCategory },
  { path: '/material', name: 'Material', component: Material },
  { path: '/add-material', name: 'Add Material', component: AddMaterial },
  { path: '/detail-material', name: 'Detail Material', component: DetailMaterial },
  { path: '/profile/companies', name: 'Company', component: Company },
  { path: '/profile/add-companies', name: 'Add Company', component: AddCompany },
  { path: '/profile/detail-companies', name: 'Detail Company', component: DetailCompany },
  { path: '/profile/company-properties', name: 'Company Properties', component: CompanyProperties },
  { path: '/profile/add-company-properties', name: 'Add Company Properties', component: AddCompanyProperties },
  { path: '/profile/detail-company-properties', name: 'Detail Company Properties', component: DetailCompanyProperties },
  { path: '/profile/company-units', name: 'Company Units', component: CompanyUnits },
  { path: '/profile/add-company-units', name: 'Add Company Units', component: AddCompanyUnits },
  { path: '/profile/detail-company-units', name: 'Detail Company Units', component: DetailCompanyUnits },
  { path: '/profile/owner-tenant', name: 'Owner Tenant', component: OwnerTenant },
  { path: '/profile/add-owner-tenant', name: 'Add Owner Tenant', component: AddOwnerTenant },
  { path: '/profile/detail-owner-tenant', name: 'Detail Owner Tenant', component: DetailOwnerTenant },
  { path: '/tenancy/unit-rent', name: 'Unit Rent', component: UnitRent },
  { path: '/tenancy/detail-unit-rent', name: 'Detail Unit Rent', component: DetailUnitRent },
  { path: '/maintenance', name: 'Maintenance', component: Maintenance },
  { path: '/add-maintenance', name: 'Add Maintenance', component: AddMaintenance },
  { path: '/detail-maintenance', name: 'Detail Maintenance', component: DetailMaintenance },
  { path: '/transactions/billings', name: 'Billings', component: Billings },
  { path: '/transactions/detail-billings', name: 'Detail Billings', component: DetailBillings },
  { path: '/transactions/invoices', name: 'Invoices', component: Invoices },
  { path: '/transactions/detail-invoices', name: 'Detail Invoices', component: DetailInvoices },
  { path: '/transactions/history-invoices', name: 'History Invoices', component: HistoryInvoices },
];

export default routes;
