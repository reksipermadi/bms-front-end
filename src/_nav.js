export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'cui-dashboard',
    },
    {
      title: true,
      name: 'Settings',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'AppConfig',
      url: '/settings/appconfig',
      icon: 'icon-settings',
    },
    {
      name: 'MasterData',
      url: '/masterdata',
      icon: 'icon-note',
      children: [
        {
          name: 'Province',
          url: '/masterdata/province',
          icon: 'icon-directions',
        },
        {
          name: 'City',
          url: '/masterdata/city',
          icon: 'icon-directions',
        },
        {
          name: 'Country',
          url: '/masterdata/country',
          icon: 'icon-directions',
        },
      ],
    },
    {
      title: true,
      name: 'General Setup',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Account',
      url: '/account',
      icon: 'icon-user',
      children: [
        {
          name: 'Chart of Account',
          url: '/account/chart-of-account',
          icon: 'icon-chart',
        },
        {
          name: 'Bank Account',
          url: '/account/bank-account',
          icon: 'icon-credit-card',
        },
        {
          name: 'Opening Balance',
          url: '/account/opening-balance',
          icon: 'icon-envelope-open',
        },
      ],
    },
    {
      name: 'Budget',
      url: '/budget',
      icon: 'cui-dollar',
      children: [
        {
          name: 'Budget Group',
          url: '/budget/budget-group',
          icon: 'icon-docs',
        },
        {
          name: 'Budget Settings',
          url: '/budget/budget-settings',
          icon: 'icon-settings',
        },
      ],
    },
    {
      name: 'Complaints',
      url: '/complaints',
      icon: 'icon-info',
      children: [
        {
          name: 'Complaint Category',
          url: '/complaint/complaint-category',
          icon: 'icon-info',
        },
        {
          name: 'Complaint Issue',
          url: '/complaint/complaint-issue',
          icon: 'icon-info',
        },
      ],
    },
    {
      name: 'Organizations',
      url: '/organizations',
      icon: 'icon-people',
      children: [
        {
          name: 'Division',
          url: '/organization/division',
          icon: 'icon-people',
        },
        {
          name: 'Department',
          url: '/organization/department',
          icon: 'icon-people',
        },
      ],
    },
    {
      name: 'Invoices',
      url: '/invoices',
      icon: 'icon-doc',
      children: [
        {
          name: 'Invoice Group',
          url: '/invoice/invoice-group',
          icon: 'icon-docs',
        },
        {
          name: 'Invoice Category',
          url: '/invoice/invoice-category',
          icon: 'icon-docs',
        },
      ],
    },
    {
      name: 'Meters',
      url: '/meters',
      icon: 'icon-speedometer',
      children: [
        {
          name: 'Meter Group',
          url: '/meters/meter-group',
          icon: 'icon-speedometer',
        },
        {
          name: 'Meter',
          url: '/meter',
          icon: 'icon-speedometer',
        },
        {
          name: 'Meter Mapping',
          url: '/meters/meter-mapping',
          icon: 'icon-speedometer',
        },
      ],
    },
    {
      name: 'Services',
      url: '/services',
      icon: 'icon-info',
      children: [
        {
          name: 'Service Group',
          url: '/service/service-group',
          icon: 'icon-info',
        },
        {
          name: 'Service Settings',
          url: '/service/service-setting',
          icon: 'icon-info',
        },
      ],
    },
    {
      name: 'Suppliers',
      url: '/suppliers',
      icon: 'icon-people',
      children: [
        {
          name: 'Supplier Group',
          url: '/suppliers/supplier-group',
          icon: 'icon-people',
        },
        {
          name: 'Supliers',
          url: '/supplier',
          icon: 'icon-people',
        },
      ],
    },
    {
      name: 'User Management',
      url: '/user-management',
      icon: 'icon-people',
      children: [
        {
          name: 'User Roles',
          url: '/user-management/user-roles',
          icon: 'icon-people',
        },
        {
          name: 'User Account',
          url: '/user-management/user-account',
          icon: 'icon-people',
        },
      ],
    },
    {
      name: 'Doc Number format',
      url: '/doc-number-format',
      icon: 'icon-doc',
    },
    {
      name: 'Tax Settings',
      url: '/tax-settings',
      icon: 'icon-settings',
    },
    {
      name: 'Warehouse',
      url: '/warehouse',
      icon: 'icon-home',
    },
    {
      name: 'Leasable Area',
      url: '/leasable-area',
      icon: 'icon-map',
    },
    {
      title: true,
      name: 'Items',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Equipments',
      url: '/equipments',
      icon: 'icon-wrench',
      children: [
        {
          name: 'Brands',
          url: '/equipments/brands',
          icon: 'icon-bag',
        },
        {
          name: 'Equipment Category',
          url: '/equipments/equipment-category',
          icon: 'icon-wrench',
        },
        {
          name: 'Equipment',
          url: '/equipment',
          icon: 'icon-wrench',
        },
      ],
    },
    {
      name: 'Materials',
      url: '/materials',
      icon: 'icon-layers',
      children: [
        {
          name: 'Materials Category',
          url: '/materials/material-category',
          icon: 'icon-layers',
        },
        {
          name: 'Materials',
          url: '/material/',
          icon: 'icon-layers',
        },
      ],
    },
    {
      title: true,
      name: 'Profile',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Companies',
      url: '/profile/companies',
      icon: 'icon-briefcase',
    },
    {
      name: 'Company Properties',
      url: '/profile/company-properties',
      icon: 'icon-briefcase',
    },
    {
      name: 'Company Units',
      url: '/profile/company-units',
      icon: 'icon-briefcase',
    },
    {
      name: 'Owner & Tenant',
      url: '/profile/owner-tenant',
      icon: 'icon-people',
    },
    {
      title: true,
      name: 'Tenancy',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Unit Rent',
      url: '/tenancy/unit-rent',
      icon: 'icon-briefcase',
    },
    {
      title: true,
      name: 'Maintenances',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Maintenance',
      url: '/maintenance',
      icon: 'icon-wrench',
    },
    {
      title: true,
      name: 'Transactions',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Billings',
      url: '/transactions/billings',
      icon: 'icon-calculator',
    },
    {
      name: 'Invoices',
      url: '/transactions/invoices',
      icon: 'icon-doc',
    },
    {
      title: true,
      name: 'Accounting',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Journal',
      url: '/journal',
      icon: 'cui-dollar',
    },
  ],
};
